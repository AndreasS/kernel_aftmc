# kernel_aftmc

This projects intends to create a Visualization (svg) which aims to connect the kernel syllabus - https://kernel.community/en/learn/ - https://github.com/kernel-community/kernel-v2 (books mentioned) with books mentioned in john vervaekes awakening from the meaning crisis

Zetteklasten note for aftmc books:

# Awakening from the meaning Crisis - Notes - Book References

#tags: #aftmc #john_vervaeke #notes #future_thinkers #discord
- future thinkers until episode 18: https://docs.google.com/document/d/1WhacRQY-Cv34HSnfAAnBJEABET-8I__Q7RNkypf43QU/edit#heading=h.pajraxiqf14v
- https://docs.google.com/document/d/1VEhfb09YIZLW1a__Ck4uq_618SYP4fwW5b3YimZaux4/edit#heading=h.l6kj7qiae4t0 Pleasure of Doubt POD aftmc discord notes
- https://www.meaningcrisis.co/ep-38-awakening-from-the-meaning-crisis-agape-and-4e-cognitive-science/
- someone from the discord shared this book list: https://www.goodreads.com/list/show/149801.John_Vervaeke_s_Awakening_From_the_Meaning_Crisis
- Valeria shared this spread sheet with book references from aftmc and John vervaeke/cognitive science in general: https://docs.google.com/spreadsheets/d/1b35239242i7PfmN7tkNkcCUflOMjUNh-6s1MGdlVGII/edit#gid=8057604

# Kernel good reads Bibliography

- https://www.goodreads.com/review/list/98176865-andy-tudhope?ref=nav_mybooks&shelf=kernel
