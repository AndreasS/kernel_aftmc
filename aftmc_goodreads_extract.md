How Enlightenment Changes Your Brain The New Science of Transformation by Andrew B. Newberg
Flow The Psychology of Optimal Experience by Mihaly Csikszentmihalyi
The Anti-Christ by Friedrich Nietzsche
Natural-Born Cyborgs Minds, Technologies, and the Future of Human Intelligence by Andy Clark
Systematic Theology by Paul Tillich
The Reasons of Love by Harry G. Frankfurt
Hypnosis and Meditation Towards an Integrative Science of Conscious Planes by Amir Raz
Knowing and Being by Michael Polanyi
The Great Transformation The Beginning of Our Religious Traditions by Karen Armstrong
Philosophy as a Way of Life Spiritual Exercises from Socrates to Foucault by Pierre Hadot
Imaginal Love The Meanings of Imagination in Henry Corbin and James Hillman by Tom Cheetham
How to Be a Stoic Using Ancient Philosophy to Live a Modern Life by Massimo Pigliucci
Platonic Mysticism Contemplative Science, Philosophy, Literature, and Art by Arthur Versluis
Wonder From Emotion to Spirituality by Robert C. Fuller
Moby-Dick or, the Whale by Herman Melville
Supernatural Selection How Religion Evolved by Matt Rossano
The Dhammapada by Anonymous
The Mindfulness Revolution Leading Psychologists, Scientists, Artists, and Meditatiion Teachers on the Power of Mindfulness in Daily Life by Barry Boyce
The Awakening of the West The Encounter of Buddhism and Western Culture by Stephen Batchelor
A History of the World in 10½ Chapters by Julian Barnes
Beyond Fate(Massey Lectures) (CBC Massey Lecture) by Margaret Visser
Breakdown of Will by George Ainslie
The Gnostic New Age How a Countercultural Spirituality Revolutionized Religion from Antiquity to Today by April D DeConick
The Cambridge Handbook of Consciousness by Philip David Zelazo
World Turned Inside Out Henry Corbin and Islamic Mysticism by Tom Cheetham
Your Brain Is(Almost) Perfect How We Make Decisions by Read Montague
The Better Angels of Our Nature Why Violence Has Declined by Steven Pinker
The Interpretation of Cultures by Clifford Geertz
Cognitive Psychology A Student's Handbook by Michael W. Eysenck
A Cognitive Theory Of Consciousness by Bernard J. Baars
The New Gnosis Heidegger, Hillman, and Angels by Robert Avens
Shamanism A Biopsychosocial Paradigm of Consciousness and Healing, 2nd Edition by Michael Winkelman
Memoirs of an Addicted Brain A Neuroscientist Examines his Former Life on Drugs by Marc Lewis
Intuition Pumps And Other Tools for Thinking by Daniel C. Dennett
Twilight of the Idols by Friedrich Nietzsche
Phenomenology of Perception by Maurice Merleau-Ponty
Metaphors We  Live By by George Lakoff
The Principles of Art by R.G. Collingwood
Attention Is Cognitive Unison An Essay in Philosophical Psychology by Christopher Mole
Sacred Reading The Ancient Art of Lectio Divina by Michael Casey
Things and Places How the Mind Connects with the World by Zenon W. Pylyshyn
Dynamics in Action Intentional Behavior as a Complex System by Alicia Juarrero
The Sovereignty of Good (Routledge Classics) by Iris Murdoch
To Have or to Be? The Nature of the Psyche by Erich Fromm
The Protestant Ethic and the Spirit of Capitalism by Max Weber
What Is Ancient Philosophy? by Pierre Hadot
On Beauty and  Being Just by Elaine Scarry
The Scientific Study of Personal Wisdom From Contemplative Traditions to Neuroscience by Michel Ferrari
Minimal Rationality by Christopher Cherniak
After Buddhism Rethinking the Dharma for a Secular Age by Stephen Batchelor
Practical Induction by Elijah Millgram
Educating Intuition by Robin M. Hogarth
Thinking Being Introduction to Metaphysics in the Classical Tradition by Eric Perl
The Courage to Be by Paul Tillich
All the World an Icon Henry Corbin and the Angelic Function of Beings by Tom Cheetham
Quantum Change When Epiphanies and Sudden Insights Transform Ordinary Lives by William R. Miller
The Tacit Dimension by Michael Polanyi
The View from Nowhere by Thomas Nagel
Relevance Communication &amp;amp; Cognition by Dan Sperber
Implicit  Learning and Tacit Knowledge An Essay on the Cognitive Unconscious by Arthur S. Reber
After  Phrenology Neural Reuse and the Interactive Brain by Michael L. Anderson
PLOTINUS Ennead V.1 On the Three Primary Levels of Reality Translation, with an Introduction and Commentary by Eric D. Perl
Transcendence and Self-Transcendence On God and the Soul by Merold Westphal
The Mind in the Cave Consciousness and the Origins of Art by David Lewis-Williams
Waking From Sleep Why Awakening Experiences Occur And How To Make Them Permanent by Steve Taylor
The Embodied Mind Cognitive Science and Human Experience by Francisco J. Varela
Retrieving Realism by Hubert L. Dreyfus
The Origin And Goal Of History by Karl Jaspers
Man and His Symbols by C.G. Jung
The  Ecological Approach to Visual Perception by James J. Gibson
Structuring Mind The Nature of Attention and how it Shapes Consciousness by Sebastian Watzl
Buddhism without Beliefs A Contemporary Guide to Awakening by Stephen Batchelor
Mindsight The New Science of Personal Transformation by Daniel J. Siegel
Alone with Others An Existential Approach to Buddhism by Stephen Batchelor
The Perennial Philosophy by Aldous Huxley
I Don't Believe in Atheists by Chris Hedges
After Phrenology Neural Reuse and the Interactive Brain by Michael L. Anderson
The End of the Bronze Age Changes in Warfare and the Catastrophe ca. 1200 B.C. by Robert Drews
The Gnostic Religion The Message of the Alien God and the Beginnings of Christianity by Hans Jonas
The Joy of Secularism 11 Essays for How We Live Now by George Lewis Levine
Zombies in Western Culture A Twenty-First Century Crisis by John Vervaeke
The Axial Age and Its Consequences by Robert N. Bellah
1177 B.C. The Year Civilization Collapsed by Eric H. Cline
The Confessions of Saint Augustine by Augustine of Hippo
Mind, Reason,  and Being-in-the-World The McDowell-Dreyfus Debate by Joseph K. Schear
The Problem of Pure Consciousness Mysticism and Philosophy by Robert K.C. Forman
Phi A Voyage from the Brain to the Soul by Giulio Tononi
American Fascists The Christian Right and the War On America by Chris Hedges
Meaning in Life and Why It Matters by Susan R. Wolf
Descartes' Error Emotion, Reason and the Human Brain by António R. Damásio
Lost Knowledge of the Imagination by Gary Lachman
