<Link to="https://she256.org/">she(256)</Link> and <Link to="https://metagammadelta.com/">MetaGammaDelta</Link>.**
1. <Link to="https://blog.oceanprotocol.com/a-wholistic-overview-of-the-ocean-ecosystem-dbb3a803096b">A Wholistic Overview of the Ocean Ecosystem</Link>
2. <Link to="https://blog.oceanprotocol.com/towards-a-practice-of-token-engineering-b02feeeff7ca?gi=d33bffc65708">Towards a Practice of Token Engineering</Link>
3. <Link to="https://blog.oceanprotocol.com/the-web3-sustainability-loop-b2a4097a36e">The web3 Sustainability Loop</Link>
<Link to="https://medium.com/@trentmc0/wild-wooly-ai-daos-d1719e040956">
<Link to="https://blog.simondlr.com/posts/decentralized-autonomous-artists">
<Link to="https://medium.com/@genekogan/artist-in-the-cloud-8384824a75c7">
<Link to="https://algovera.notion.site/Artificial-Intelligence-7f57df2343e64ed9984a8f0b10dc0607">
<Link to="https://discord.com/invite/e65RuHSDS5">
<Link to="https://www.algovera.ai/">
This image is taken from the work of Nicky Case, who helpfully reminds us that change usually comes from <Link to="https://blog.ncase.me/evolution-not-revolution/">Evolution, Not Revolution.</Link> 
<Link to="https://store.steampowered.com/app/1070710/Kind_Words_lo_fi_chill_beats_to_write_to/">
<Link to="https://www.gdcvault.com/play/1023329/Engines-of-Play-How-Player">
This GDC talk by <Link to="https://twitter.com/the_darklorde">Jason VandenBerghe</Link>, designer of Ubisoft's _For Honor_, is a great primer on how psychology affects game design. 
<Link to="https://www.gamasutra.com/view/feature/129948/the_chemistry_of_game_design.php?page=1">
Written by <Link to="https://twitter.com/danctheduck">Daniel Cook</Link> about a decade before Jason's _Engines of Play_. It's an earlier look into how game designers were thinking about games in a time before app stores and the current attention economy. 
<Link to="https://steamcommunity.com/profiles/76561198024087514/recommended/8500/">
<Link to="https://tonysheng.substack.com/">
* <Link to="https://tonysheng.substack.com/p/game-economies">Do Open In-Game Economies Make Games Less Fun</Link>
* <Link to="https://tonysheng.substack.com/p/vitaliks-warlock-probably-deserved">Vitalik's Warlock Probably Deserved It</Link>
<Link to="https://mitpress.mit.edu/books/virtual-economies">
<Link to="https://play.google.com/store/apps/details?id=com.innersloth.spacemafia&hl=en&gl=US">
<Link to="https://twitter.com/elistonberg/status/1461342536954486787?s=20">
<Link to="https://tonysheng.substack.com/p/the-virtual-worlds-future-is-already">
<Link to="https://en.wikipedia.org/wiki/Cicada_3301">
1. <Link to="https://helium.com/">https://helium.com/</Link>
2. <Link to="https://explorer.helium.com/">https://explorer.helium.com/</Link>
3. <Link to="https://docs.helium.com/blockchain/proof-of-coverage/">Proof of Coverage</Link>
4. <Link to="https://coinmarketcap.com/currencies/helium/">CoinmarketCap Overview</Link>
The collaborative whiteboard for the session can be found <Link to="https://www.figma.com/file/NbH0TmgPSQZ9Xw1j7HNUt5/Tokens-Guild%3A-Helium---Nov-15-2021">here</Link>.
<Link to="https://tecommons.org/">
<Link to="https://config.tecommons.org/config/1">
<Link to="https://sim.commonsstack.org/">
<Link to="https://en.wikipedia.org/wiki/Rochdale_Principles">
from the <Link to="https://harolddavis3.github.io/Two-Row-Wampum-Social-Layer-Platform">Two Row Wampum Working Group</Link> and the historical Longhouse Council of the Five Nations Confederacy participating with us, and below you can find a summary of some of the resources they have made available. We do not need to reinvent the wheel - we only need to rediscover our shared roots. In truth, there is only one human family.
The <Link to="https://orionmagazine.org/article/the-rights-of-the-land/">transformative power of indigenous dialogue</Link> resides largely in the protocols for reaching 
flown <Link to="https://www.seashepherd.org.uk/news-and-commentary/news/sea-shepherd-receives-the-flag-of-the-five-nations-of-the-iroquois-confederacy.html">on all seas</Link>, 
Enter <Link to="https://www.researchgate.net/publication/27224530_Toward_an_Anthropological_Theory_of_Value_The_False_Coin_of_Our_Own_Dreams">David Graeber</Link>:
— <Link to="https://a.co/9nnyt8e">Words of Peace in Native Land</Link>: 
and <Link to="https://en.wikipedia.org/wiki/Songline">communicating the lines</Link> which lead to us, here and now, we can protect against nepotism 
<Link to="https://conversational-leadership.net/junto-club/">
<Link to="https://s3.us-west-2.amazonaws.com/secure.notion-static.com/56a5d908-c0fd-42c7-90a0-73babf88fdf2/Ben-Franklin-Circles-Toolkit-for-Hosts.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20210720%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20210720T152146Z&X-Amz-Expires=86400&X-Amz-Signature=9a9944b90ee88a148f702c13276e14515f6d672866332e1c04228a3a42c44f49&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22Ben%2520Franklin%2520Circles%253A%2520Toolkit%2520for%2520Hosts.pdf%22">
<Link to="/">
<Link to="https://makerdao.com/">
<Link to="/">
<Link to="https://makerdao.com/">
<Link to='/'>Link </Link>
The only way is through <Link to="https://libraryofbabel.info">the universal library</Link>. 
Every word you could possibly write is already in there: the <Link to="https://libraryofbabel.info/bookmark.cgi?bitcoin:3">bitcoin whitepaper</Link>, the <Link to="https://libraryofbabel.info/bookmark.cgi?genesis">bible</Link>, the <Link to="https://libraryofbabel.info/bookmark.cgi?playwithpattern">Kernel syllabus</Link>, the true story of your own death, the Universal Declaration of Peace, every poem, every love song, every sick note you ever handed in to try and skip school because they never taught you how to truly learn... Literally everything that could ever be strung together is already there. The problem, of course, is finding the right signal in such an infinite array of noise.
We'll start simply. Anyone who is able to <Link to="https://libraryofbabel.info/bookmark.cgi?truthseeds">solve our series of riddles</Link> - based on the content from [week 0](/learn/module-0) - will become the steward of <Link to="https://opensea.io/assets/0x495f947276749ce646f68ac8c248420045cb7b5e/43060612980525300798945617690241154227807202326827270128619443670706517180417">this one-of-a-kind NFT</Link>, which blends the _I Ching_ hexagrams with a circular representation of our genetic code. All further instructions are to be found in the Library itself.
It seems we started too simply, and the first koan was <Link to="https://twitter.com/youjustwin42/status/1348825535812571136">solved</Link> within a day of being made public. Which is wonderful, because now we get to make more. Here is <Link to="https://libraryofbabel.info/bookmark.cgi?truthsparks">the next series of riddles</Link> for you intrepid explorers of the infinite. This time, <Link to="https://opensea.io/assets/0x495f947276749ce646f68ac8c248420045cb7b5e/43060612980525300798945617690241154227807202326827270128619443671806028808193">the reward</Link> is an unpublished poem-as-NFT which will guide you to the beginning of a rabbit hole about The Land of Nowhere.
For those who make it through, there is a unique NFT courtesy of Adrian le Bas and the <Link to="https://ethblock.art/view/117">ethblock.art</Link> team. It is a rare, budding Maurer Rose, uniquely generated from Block 1987064.
The riddle begins <Link to="https://libraryofbabel.info/bookmark.cgi?truthbud">here</Link>. _Hamba kahle_.
The Module 3 koan begins <Link to="https://libraryofbabel.info/bookmark.cgi?truthmeans">here</Link>. The prize this week is called <Link to="https://ethblock.art/view/106">"Candid Syzygy"</Link>. The word "syzygy" (along with "rhythms") is the longest word without vowels in English. A syzygy is a roughly straight-line configuration of three or more celestial bodies in a gravitational system, an alignment of the stars from which you are made: those points of past light which guide us into a common future.
_We have <Link to="https://thebluebook.co.za/four-part-harmony/">remembered</Link>._  
The koan for this last week begins <Link to="https://libraryofbabel.info/bookmark.cgi?trueheart">here</Link>. The NFT prize is generated from the style "Infinite Beauty" on ethblock.art and is called <Link to="https://ethblock.art/view/266">HeartMap</Link>. The naming of the sidebar here has changed with the new website, so you should know that there are koans for Modules 1, 3 and 7 if you're going to attempt to solve this. Good luck!
This week, we travel with <Link to="https://www.brainpickings.org/2019/10/23/the-weighing-jane-hirshfield/">Jane Hirschfield</Link>:
Code, law, and how to <Link to="https://www.poetryfoundation.org/poems/54897/the-layers">live in all the layers</Link> between.
back to 1879 with Vincent Van Gogh - who painted <Link to="https://www.brainpickings.org/2014/11/13/van-gogh-starry-night-fluid-dynamics-animation/" >turbulent flow</Link> 
There's an internet-famous movie called <Link to="https://www.youtube.com/watch?v=ByY6j0qzOyM" >Good Copy Bad Copy</Link> 
This has been true since Phil Zimmerman published the source code for <Link to="https://books.google.co.za/books/about/PGP_Source_Code_and_Internals.html?id=xR4ZAQAAIAAJ&redir_esc=y" >PGP as a book</Link> 
The <Link to="https://en.wikipedia.org/wiki/Octahedron">octahedron</Link> is the third 
of the <Link to="https://en.wikipedia.org/wiki/Platonic_solid">five solids</Link> postulated 
the <Link to="https://en.wikipedia.org/wiki/Noble_Eightfold_Path" >Noble Eightfold Path</Link>. 
More practically, <Link to="https://en.wikipedia.org/wiki/Space_frame">space frames</Link> are a commonly used architectural design, extended into <Link to="https://tinyurl.com/mister-fuller">octagonal trusses by Buckminster Fuller</Link> in his work on geodesic domes. Fuller is often quoted in crypto circles, with this being the go-to choice 
<Link to="https://www.youtube.com/watch?v=kffo3pxNO7c" >ee cummings</Link>
<Link to="https://www.youtube.com/watch?v=54l8_ewcOlY&list=PLND1JCRq8Vuh3f0P5qjrSdb5eC1ZfZwWJ">
<Link to="https://youtu.be/R7FjX0GEiAM?t=110">
instruments they're using are things like Repurchase Agreements, Eurodollars (or <Link to="https://www.bloomberg.com/news/articles/2021-01-12/global-banks-warn-of-market-chaos-if-court-were-to-abolish-libor">LIBOR</Link>), and 
highlighted in the <Link to="https://en.wikipedia.org/wiki/Triffin_dilemma" >Triffin dilemma</Link>. 
please <Link to="https://www.amazon.com/Lords-Finance-Bankers-Broke-World/dp/0143116800" >read this book</Link>. 
Watch <Link to="https://www.youtube.com/watch?v=AGkH0w9hpSQ&list=PLmtuEaMvhDZZyyAkEniNUMTTnlJj6qiKW&index=4" >this video</Link> 
Watch <Link to="https://www.youtube.com/watch?v=5Fa-9RTK8zc&list=PLmtuEaMvhDZYfVv95KDQWd8-7UrJCJ9Pm&index=6" >these videos</Link> 
Watch <Link to="https://www.youtube.com/watch?v=PyTGohPHqy0&list=PLmtuEaMvhDZZh6zLsntFVt3UIgQ3VqvVv&index=2" >this part of the course</Link> 
from the videos starting <Link to="https://www.youtube.com/watch?v=5oriNHybOsc&list=PLmtuEaMvhDZaBnW0RhzOOk6FO91MdsSEc&index=2" >here</Link> - 
<Link to="https://thebitcoinpodcast.com/">Corey Petty</Link> and Justin B question the claim made in point 3 above. The debate boils down to whether asking good questions in general is an ability best developed through disciplined exercise of rational skill, or the cultivation of "beginner's mind". In particular, techniques like <Link to="https://en.wikipedia.org/wiki/Socratic_method">the Socratic Method</Link> develop rational inquiry in order to fuel deeper, more intuitive understanding of a given topic. Corey writes:
However, "beginner's mind" is not opposed to rational inquiry and the honing of skill: it is, in fact, the pinnacle of such discipline. Consider the <Link to="https://en.wikipedia.org/wiki/Socratic_method#Questioning_methods_in_Socratic_Circles">Socratic method</Link> more closely:
Rational inquiry demands a diagnosis; a solution; an end to a process which had its beginning in the mind. What can such a mind do in the face of "no single right answer"? It can develop <Link to="https://en.wikipedia.org/wiki/Negative_capability">negative capability</Link>, but even this is insufficient, because we're not after skillful intellectual understanding: we seek harmonious and clear _ways of living_. And life is not only rational. 
<Link to="https://kortina.nyc/essays/principles-for-radical-tax-reform-and-a-universal-dividend/">
<Link to="https://theconvivialsociety.substack.com/p/the-paradox-of-control">
<Link to="https://www.youtube.com/watch?v=rAL3L4-7r0M">
- **Per se** defamation can be more elegantly handled by <Link to="https://observablehq.com/@andytudhope/embedded-discover" >mechanisms</Link> like that which runs <Link to="https://dap.ps">dap.ps</Link>. Anyone can vote in a way which "defames" a product or service, but that vote is also an economic signal denoted in tokens, which are sent straight back to the product or service. So, your downvote causes reputational damage while *simultaneously* repaying the people you're damaging (programmed according to a curve which makes votes cheaper the richer/higher the rank of whomever you're voting on is).
- **Per quod** defamation can be more elegantly handled either by some version of <Link to="https://gitcoin.co/blog/gitcoin-grants-round-5-funding-our-future/" >negative votes</Link> Gitcoin Grants is currently iterating over, or by [collections of composable identities](/learn/module-4/self-enquiry/) - yet to mature.
The (in)famous <Link to="https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1559.md">EIP-1559</Link> is a great example of how we price transactional expression at the protocol level, which is where the most contentious <Link to="https://github.com/ethereum/EIPs/blob/master/EIPS/eip-2593.md" >debates</Link> occur. Note, however, how such debate centres not on opinion, but engineering <Link to="/learn/module-4/consensus/#1-lack-of-disagreement-is-more-important-than-agreement">trade-offs</Link> and technical proof.
<Link to="https://www.circularconversations.com/conversations/metamodern-values-for-a-listening-society">
is the <Link to="https://www.oreilly.com/openbook/freedom/ch07.html">only kind of freedom</Link> we can trust to generate sustainable value.
Happily, we have great historical precedent on which to lean and from which to learn. One of the root documents of the <Link to="http://www.gnu.org/philosophy/philosophy.html">free software</Link> movement is the <Link to="http://www.gnu.org/gnu/manifesto.html">GNU Manifesto</Link>, written by Richard Stallman in 1985. Rather than summarizing it here, we wish simply to note three fascinating sentences which we feel inform the underlying ideals of this document, and the many people inspired by it.
<Link to="https://www.gvsu.edu/cms4/asset/843249C9-B1E5-BD47-A25EDBC68363B726/grandrapidspress_2017-sep_14_from_the_earth_-_humus_humanity_humility.pdf" >
book <Link to="https://www.brainpickings.org/2020/05/17/yes-to-life-in-spite-of-everything-viktor-frankl/" >Man's Search for Meaning</Link>, 
linguistic primitive. Few understand <Link to="https://unifying.thebluebook.co.za/?stackedPages=%2Fstructure">this</Link>, similar to how hyperlinks were misunderstood early in protocol development. 
In a way, the internet is becoming a nation. See J. P. Barlow's <Link to="https://www.eff.org/cyberspace-independence">Declaration of the Independence of Cyberspace</Link>.
an <Link to="https://github.com/norvig/pytudes/blob/master/ipynb/Economics.ipynb">open-source Jupyter notebook</Link>, you can test it yourself, and are invited to find initial distributions which do effect long-term inequalities if you can. The authors call this
<Link to="http://www.rheingold.com/texts/tft/12.html#Chap12">
Rushkoff went on to extend these ideas in <Link to="https://rushkoff.com/books/team-human-book/" >Team Human</Link>, which is another fantastic read for any budding media theorist. His voice is a critical counterpoint to Silicon Valley singularity worship.
> "The book is a humanist work, and I'm declaring myself with <Link to="https://tinyurl.com/agent-alien" >Jaron</Link> <Link to="https://www.youtube.com/watch?v=IwbGumZ-FYg" >Lanier</Link> and other humanists. I do believe there is something special about people that we don't quite understand and that our efforts to upload ourselves or simulate our realities will fall short of what it actually is to be human. **Our best defense against present shock is to be in the genuine present**."
and <Link to="https://www.goodreads.com/book/show/83533.The_Age_of_Spiritual_Machines">The Age of Spiritual Machines</Link> and all the 'bizarre singularity stuff' is one and the same. However, there is a critical difference: Rushkoff is saying we, us messy human beings, have to find our own way into the genuine present. Kurzweil is saying that the machines will do it for us. Perhaps the truth is somewhere in the middle?
the answer is the same: to fall back into the present and to <Link to="https://youtu.be/VQZ8oPXgK8o?t=479">be here now</Link>. But the trick to being here now in an industrial era is different than the trick to being here now in a digital era.
<Link to="https://www.youtube.com/watch?v=YmR3oR4yeGg">
    <Link to="https://download.tuxfamily.org/openmathdep/history/Celebration_of_Awareness-Illich.pdf">
    <Link to="https://two.compost.digital/clock-basket-no-1/">
<Link to="https://aeon.co/essays/art-like-loss-can-shock-us-into-our-authentic-self">
In a <Link to="https://breakingsmart.com/en/season-1/rough-consensus-and-maximal-interestingness/">different article</Link>, Venkatesh Rao discusses how:
don't <Link to="https://www.patreon.com/posts/online-community-47388851">need principles</Link>, 
<Link to="https://docs.google.com/document/d/1n2ntDBQQj0VrbBUE4UXC4zx7iJWrZTQUJQffDLkuo8E/edit?usp=sharing">
<Link to="https://vitalik.ca/general/2020/08/17/philosophy.html">
 -- <Link to="https://tinyurl.com/le-guins-tao">Lao Tzu</Link>
<Link to="https://interdependence.fm/episodes/radical-transparency-humor-disinformation-poetry-for-machines-avatar-politicians-and-giving-non-human-entities-a-vote-with-digital-minister-of-taiwan-audrey-tang-X36XnWVg
<Link to="https://gitcoin.co/blog/towards-computer-aided-governance-of-gitcoin-grants/">
<Link to="https://blog.makerdao.com/the-maker-foundation-returns-dev-fund-holdings-to-the-dao/">
This story is known as <Link to="https://fs.blog/2020/03/chestertons-fence/" >Chesterton's Fence</Link> 
of <Link to="https://ciechanow.ski/lights-and-shadows/" >light</Link>. The moral is: 
term <Link to="https://abahlali.org/files/Graeber.pdf">"counterpower"</Link>:
means put forward so far is the mechanism of <Link to="https://gitcoin.co/blog/experiments-with-liberal-radicalism/" >Liberal Radicalism</Link>, whose goal is "to create a funding system that is as flexible and responsive as the market, but avoids free-rider problems".
Here's the key idea: if individuals contribute to the public goods they use **and** the <Link to="https://youtu.be/hEHv-dE4xl8?t=274">funding principle underlying the market is nonlinear</Link>, then we can make sure that "small contributions are heavily subsidized (as these are the most likely to be distorted by free-riding incentives) while large ones are least subsidized, as these are more likely private goods."
though: <Link to="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3243656">the math</Link> 
<Link to="https://gitcoin.co/blog/how-to-attack-and-defend-quadratic-funding/">
<Link to="https://gitcoin.co/blog/token-engineering-open-science-program-a-multidisciplinary-study-of-gitcoin-grants/">
<Link to="https://vitalik.ca/general/2021/04/02/round9.html">
<Link to="https://vitalik.ca/general/2020/10/18/round7.html">
<Link to="https://vitalik.ca/general/2020/07/22/round6.html">
<Link to="https://vitalik.ca/general/2020/04/30/round5.html">
Almost everything we've discussed so far - [trust](/learn/module-0/trust/), [value](/learn/module-1/value/), [meaning](/learn/module-1/meaning/), [speech](/learn/module-2/money-speech/), [freedom](/learn/module-3/freedom/), [intention](/learn/module-3/intention/), [humility](/learn/module-3/humility/) and [art](/learn/module-4/art/) - depends in some way on the question of identity. The <Link to="https://web.archive.org/web/20181024152027/http://www.whitepine.org/wildways.pdf" >Wild Ways</Link> of Ikkyu explain best:
In considering prosocial ways to protect against collusion that allow the social and financial to communicate clearly, we can look to projects like <Link to="https://joincircles.net/">CirclesUBI</Link> and the web of trust models on which it is built. Such environments allow people to signal that they trust each other sufficiently in specific applications, producing a **network of identities** linked by varying degrees of connections. However, because the signal of trust is itself generated within a wallet which can be used in other applications that all share the same protocol, these networks of identity can be used to create increasingly reciprocal communal spaces. Kei is characteristically considerate about this kind of possibility, though, making the important point that:
<Link to="https://www.youtube.com/watch?v=vD6F3rIuo5g&list=PLWzYrEdlV4O4i8fHqNOwXK9S077EA7TnE">
<Link to="https://sfhfoundation.com/audios/series/self-knowledge/">
We are <Link to="https://translation-blog.trustedtranslations.com/time-binding-animal-2015-08-03.html" >time-binding animals</Link>, 
memory <Link to="https://www.ribbonfarm.com/2020/05/07/leaking-into-the-future/" >which is destroying history</Link>, 
    <Link to="https://download.tuxfamily.org/openmathdep/history/Celebration_of_Awareness-Illich.pdf">
required within their specific <Link to="https://www.youtube.com/watch?v=-6rWqJhDv7M">time to realise the eternity of this instant</Link>.
What constitutes persuasive language? Words which perform what they say. If an <Link to="https://www-cs-faculty.stanford.edu/~knuth/smullyan.html">essay about humour</Link> is itself funny, then that essay is more likely to convince you about what humour is.
You must <Link to="https://blog.oceanprotocol.com/towards-a-practice-of-token-engineering-b02feeeff7ca">think like an engineer</Link> 
in <Link to="https://www.goodreads.com/book/show/1044900.Meditation_in_Action" >the manure of their own experience</Link>. 
<Link to="https://dlc.dlib.indiana.edu/dlc/bitstream/handle/10535/5962/Silence%252520is%252520a%252520Commons.html?sequence=1&isAllowed=y">
<Link to="https://www.humanetech.com/podcast/23-digital-democracy-is-within-reach">
<Link to="https://www.youtube.com/watch?v=qUxMdYhipvQ">
<Link to="https://www.stoepzen.co.za/talks/2016-freedom-day/Freedom-Day-2016-25-April.mp3">
— <Link to="https://www.youtube.com/watch?v=GOGru_4z1Vc" >Kurt Vonnegut</Link>, 1974
Project Horseshoe wrote about <Link to="https://www.projecthorseshoe.com/reports/ph16/ph16r4.htm" >psychology</Link> in 2016, <Link to="https://www.projecthorseshoe.com/reports/featured/ph18r8.htm" >logistics and scale</Link> in 2018, and this paper focuses on economics. It is by understanding these three fields together that we can begin to design truly interesting, prosocial mechanisms to help us map and navigate [intersubjective narrative space](/learn/module-5/the-peoples-narrative/#social-uses).
, <Link to="https://vitalik.ca/general/2017/10/17/moe.html" >sinks</Link> and 
to the software theory of <Link to="https://wiki.p2pfoundation.net/Agalmics" >agalmics</Link>: 
the <Link to="https://vitalik.ca/general/2017/10/17/moe.html" >sinks</Link> and the 
politics to achieve the same end results! We get so caught up in the <Link to="https://tinyurl.com/inertia-language">inertia of our current linguistic paradigms</Link> that we can't think creatively about how to achieve the desired results without recourse to the same old formalisms. Use games to free yourself.
a <Link to="https://tinyurl.com/ladder-abstraction">ladder of activities</Link>, 
**Facilitating expression**: through <Link to="https://our.status.im/discover-a-brave-new-curve/" >voting resources as economic tools</Link> and integrating social metrics with business success.
We could teach you about the different kinds of <Link to="https://medium.com/bollinger-investment-group/constant-function-market-makers-defis-zero-to-one-innovation-968f77022159" >constant function market makers</Link>, but these are just one particular flavour of one kind of nugget within the entire universe of applications made possible by distributed trust. We want to demonstrate *the principle* here: **virtuous games lead to valuable truth**. The better we design our games, the more clearly we can share our truths.
called <Link to="https://www.epsilontheory.com/epsilon-theory-manifesto/" >Epsilon Theory</Link>, 
with things like <Link to="https://www.vanityfair.com/news/2020/06/bankrupt-hertz-is-a-pandemic-zombie" >Hertz</Link> 
or <Link to="https://www.fool.com/investing/2020/08/02/elon-musk-was-right-teslas-stock-price-is-too-high.aspx" >TSLA</Link>.
<Link to="https://en.wikipedia.org/wiki/Stag_hunt">
<Link to="https://blog.zkga.me/announcing-darkforest">
<Link to="https://en.wikipedia.org/wiki/Foundation_series">
<Link to="https://blog.ethereum.org/2015/06/06/the-problem-of-censorship/">Vitalik has defined the ills of censorship</Link> and who are we to argue with the boy genius...<br/><br/>
both <Link to="https://vitalik.ca/general/2018/08/07/99_fault_tolerant.html">higher fault tolerance</Link> 
<Link to="https://allenfarrington.medium.com/elusive-symmetries-f2a2938a095f">
<Link to="https://twitter.com/vitalikbuterin/status/1256227159707877378">
    <Link to="https://www.poetryfoundation.org/poems/52577/monet-refuses-the-operation-56d231289e6db">
lens of the <Link to="https://andytudhope.africa/soul">soul</Link> is to have a healthy view; one 
<Link to="https://www.youtube.com/watch?v=gtTpIj2xRcM">Lorca’s main point</Link> of reference in elaborating his concept of duende was the Gypsy tradition of the "Deep Song," a predecessor to Flamenco. And Tracy Smith writes:
poet, this kind of survival is tantamount to walking, <Link to="https://living.thebluebook.co.za/love/promised_matter.html">word by word, onto a ledge of your own making</Link>. You must use the tools you brought with you, but in decidedly different and dangerous ways.
*Thanks to <Link to="https://earthchild.africa/">JJ van de Vyver</Link> for reminding me of this magic word.*
scaling and staking <Link to="https://hackmd.io/@benjaminion/eth2_news" >available</Link>, 
. <Link to="https://stackoverflow.blog/2017/05/23/stack-overflow-helping-one-million-developers-exit-vim/">Vim still famously does this</Link>. 
The <Link to="https://www.youtube.com/watch?v=3HYlbg6RKMA&t=73s">secret fire</Link> 
generation's <Link to="https://www.ribbonfarm.com/2019/01/31/elderblog-sutra-1/" >elder game</Link> 
Vitalik calls this "<Link to="https://vitalik.ca/general/2019/12/26/mvb.html" >functional escape velocity</Link>": 
, <Link to="https://www.gwern.net/Bitcoin-is-Worse-is-Better" >it is not elegant</Link> 
in <Link to="https://notes.ethereum.org/@vbuterin/rkhCgQteN?type=view#Security-models" >this section</Link>. 
do so, they earn a **base reward**, which is split into 5 parts described <Link to="https://notes.ethereum.org/@vbuterin/rkhCgQteN?type=view#Base-rewards">here</Link>. There are two features to understand about this reward: how it prevents "<Link to="https://raw.githubusercontent.com/ethereum/research/master/papers/discouragement/discouragement.pdf">griefing</Link>", and how it is calculated. 
, <Link to="https://github.com/ethereum/eth2.0-specs/blob/dev/specs/phase0/beacon-chain.md#proposer-slashings" >they get penalized</Link> 
a portion of their deposit equal <Link to="https://github.com/ethereum/eth2.0-specs/blob/dev/specs/phase0/beacon-chain.md#slashings" >to three times</Link> 
**32 ETH** minimum deposits <Link to="https://notes.ethereum.org/@vbuterin/rkhCgQteN?type=view#Why-32-ETH-validator-sizes" >strike a good balance</Link> 
current <Link to="https://notes.ethereum.org/@vbuterin/rkhCgQteN?type=view#Shuffling" >swap-or-not</Link> 
1. Having distinct shard chains and linking them into the beacon chain more rarely. This was rejected to allow for fast (single-slot) base-layer cross-shard communication capability. See <Link to="https://notes.ethereum.org/@vbuterin/HkiULaluS" >here</Link> for more.
to <Link to="https://twitter.com/VitalikButerin/status/1294461236680130560" >use co-ordination costs to our advantage</Link>.
<Link to="https://github.com/ethereum/eth2.0-specs">
<Link to="https://ethos.dev/beacon-chain/">
<Link to="https://ethereumstudymaster.com/courses/ethereum-2-0-studymaster-program/">
<Link to="https://www.reddit.com/r/ethereum/comments/o4unlp/ama_we_are_the_efs_research_team_pt_6_23_june_2021/h2oa00n/">
<Link to="https://stateful.mirror.xyz/Y1ED9RorG9OvEUXD8NBmXgYhSVhjj8H537-I2SZJkYA">
<Link to="https://vitalik.ca/general/2021/05/23/scaling.html">
<Link to="https://thebluepill.eth.link/">
<Link to="https://emergencemagazine.org/essay/the-serviceberry/" >
we would like to dedicate this module to [the intergalactic memory of David Graeber](/conversation/hospitality/). Consider taking some time to read one of his first and most moving works, <Link to="https://abahlali.org/files/Graeber.pdf">Fragments of An Anarchist Anthropology</Link>. In speaking about the imagination as a political principle, he wrote:
the <Link to="https://www.brainpickings.org/2014/07/07/buddhist-economics-schumacher/">greatest profit deeply offensive</Link>. 
There is, to quote Douglas Rushkoff, *<Link to="https://www.goodreads.com/book/show/139712.Nothing_Sacred" >Nothing Sacred</Link>*. 
What I most want to say here cannot be <Link to="https://youtu.be/7xwq1v8OD6s?t=255" >said</Link>. 
journey. We are on <Link to="https://youtu.be/3HYlbg6RKMA?t=961" >a fool's errand to give back the evil which does not belong</Link> 
> Due to this inertia, language stores and offers for communicative usage many <Link to="https://www.orwell.ru/library/essays/politics/english/e_polit/">remnants of many obsolete paradigms</Link>.
to discuss its own premise: we must <Link to="https://www.youtube.com/playlist?list=PL5ClmaG2tnPPFveZLH5lRrzV-Ndc3_Nli" >first teach it to experiment with itself</Link> in order to discover how it labors under the paradigm it ought to expose. 
Because all of these briefs form one, [connected chain](/learn/module-6/) of thought - a digital <Link to="https://en.wikipedia.org/wiki/Songline">songline</Link> of a different and human society - Brün finishes with a note about doubt, [humility](/learn/module-3/humility/) and [art](/learn/module-4/art/):
<Link to="https://thebaffler.com/salvos/whats-the-point-if-we-cant-have-fun">
this open-ended course. Thank you all, and we wish you a <Link to="https://unifying.thebluebook.co.za">peace</Link> past any understanding.
Gregory Chaitin discusses Umberto Eco's book *<Link to="https://www.goodreads.com/book/show/10513.The_Search_for_the_Perfect_Language">The Search for the Perfect Language</Link>*, and how it relates to some deep concepts in mathematics, coupled with a non-standard view on 
structure of the world, in which concepts are expressed in their <Link to="https://www.youtube.com/playlist?list=PL5ClmaG2tnPPFveZLH5lRrzV-Ndc3_Nli">direct, original</Link> format.
this (using <Link to="https://en.wikipedia.org/wiki/G%C3%B6del_numbering">Gödel numbering</Link>) reveals that we cannot capture all mathematical truth in any theory.
    <Link to="https://www.youtube.com/watch?v=FZ0w4B80uZA">
tools no-one controls creates the conditions required to fuel [lifelong learning](/learn/module-6/learn), that is to *scale ability across the species*. <Link to="https://medium.com/@VitalikButerin/the-meaning-of-decentralization-a0c92b76a274" >Language was the first logically decentralized system</Link> and increases in literacy have always resulted in large social changes; tectonic shifts in how we relate (to) our shared myths. Scaled blockchains are the evolution of logically decentralized systems for the creation, negotiation, and transfer of value.
Similarly, the meaning of *<Link to="https://en.wikipedia.org/wiki/Din_(Arabic)" >dīn</Link>* 
in Arabic is one's life-transaction. In the original <Link to="https://www.youtube.com/watch?v=-an6hvx6o-c&list=PL5ClmaG2tnPNgqWDBGCWAQxD0sYpbfPRK&index=30" >Aramaic</Link>, The Lord's Prayer reads, "Forgive us our *debts*, as we forgive those who are *indebted* to us." The root of the life we are given is a [debt upon us](/learn/module-2/debt/#debt-and-morality). The highest response possible is to forgive others and repay your share.
<Link to="https://www.youtube.com/watch?v=LwLP62fL83k&list=PL5ClmaG2tnPNgqWDBGCWAQxD0sYpbfPRK&index=33">
the <Link to="https://youtu.be/GxVaxOco7kg?t=4307" >Land of Fairie</Link>, only to 
from "<Link to="https://www.goodreads.com/book/show/7800891-common-as-air" >Common As Air</Link>", 
common <Link to="https://www.youtube.com/watch?v=Oohg3LZd898&t=1344s">Self</Link>, 
Hyde also quotes the famous book *<Link to="https://www.goodreads.com/book/show/35476.Black_Elk_Speaks" >Black Elk Speaks</Link>*, 
, <Link to="https://www.brainpickings.org/2017/03/09/atom-and-archetype-pauli-jung/" >synchronous spaces</Link> is so important: they allow more and more outsiders to join the community of circulating gift exchange without resorting to theft.
**Beginner** - <Link to="https://cryptozombies.io/">CryptoZombies</Link>. You know nothing about smart contracts or solidity. This interactive course will get you up to speed with basic concepts.
**Beginner** - <Link to="https://eth.build/">Eth.build</Link>. An educational sandbox for web3 which lets you code with blocks, much like in Scratch. Of course, you can do really awesome things with this tool, but it is a wonderful, visual programming tool which will help you develop an intuition for how smart contracts work, and how different the environment in which they live really is.
**Novice** - <Link to="https://www.youtube.com/watch?v=v_hU0jPtLto&list=PL16WqdAj66SCOdL6XIFbke-XQg2GW_Avg">Will It Scale</Link>. This is still our favourite series of YouTube tutorials, and it will introduce you to the Remix IDE and a whole host of much deeper concepts in Solidity. While watching videos rather than coding is generally not that useful, this will give you much more context for what is to come.
**Novice** - <Link to="https://www.youtube.com/channel/UCJWh7F3AFyQ_x01VKzr9eyA/videos">Smart Contract Programmer</Link>. Another wonderful YouTube channel you can learn a great deal from without any frills or hype.
**Intermediate** - <Link to="https://ethernaut.openzeppelin.com/">Ethernaut</Link>. This starts out simple, but contains many wonderful challenges and surprises for those more familiar with Solidity and the global, public context in which smart contracts live. You can find a detailed walk-through of the first twelve challenges in our [Outsmarting Contracts](/build/outsmarting-contracts) guild.
**Intermediate** - <Link to="https://speedrunethereum.com/">Speed Run Ethereum</Link>. A set of in-depth tutorials currently being built out which leverage the <Link to="https://github.com/scaffold-eth/scaffold-eth">scaffold-eth framework</Link>. The framework is specifically intended for quick experimentation. It's worth checking out all the branches in that repo for lost of different code relevant to everything from NFTs to exchanges and beyond.
**Intermediate** - <Link to="https://buildspace.so/">Build Space</Link>. Another wonderful cohort-based program that focuses exclusively on technical skills. Building with others is always much more fun and instructive.
**Advanced** - <Link to="https://moonshotcollective.space/">Moonshot Collective</Link>. You'll notice that continuing to do tutorials and toy examples will not get you passed the intermediate stage. If you want to master any skill, eventually you have to dive into unexplored parts of the ocean and shoot for the stars, without caring too much about what people will think of your mixed metaphors or strange commits.
_H/T <Link to="https://www.reddit.com/r/ethereum/comments/o4unlp/ama_we_are_the_efs_research_team_pt_6_23_june_2021/">EF Research Team</Link>_
A fascinating collaboration between many big minds. Begin with <Link to="https://otherinter.net/research/squad-wealth/">Squad Wealth</Link>. Complement with:
Enjoy two for the price of one with the PLONK paper by the AZTEC team and <Link to="https://vitalik.ca/general/2019/09/22/plonk.html">Vitalik’s comments</Link>.
Taken from this <Link to="https://github.com/gianni-dalerta/awesome-nft">awesome list</Link>.
_H/T Matt Solomon. Check out <Link to="https://medium.com/coinmonks/ethereum-security-analysis-tools-an-introduction-and-comparison-1096194e64d5">his article</Link> for more._
Two useful repos for understanding the vulnerable patterns used by not-so-smart contracts, and for <Link to="https://github.com/crytic/slither/tree/master/tests/detectors">detecting various potential vulnerabilities</Link>.
Another useful Consensys tool that's easy to integrate with frameworks like Truffle especially. Find a useful code, among many other things, <Link to="https://www.notion.so/Module-2-Automated-Tooling-22b7b1513dd247d685616d55e27b6877">here</Link>.
An introduction to mutation testing from Security Track mentor Joran Honig. You can find a detailed explanation <Link to="https://github.com/JoranHonig/publications/blob/master/practical_mutation_testing_for_smart_contracts.pdf">here</Link>.
<Link to='/blog/community/scholarships'>
> <Link to="https://tinyurl.com/interpret-the-game">Your interpretation of the game is more important than my intentions.</Link>
<Link to="https://tinyurl.com/interpret-the-game">
<Link to="https://www.gdcvault.com/play/1023329/Engines-of-Play-How-Player">
This GDC talk by <Link to="https://twitter.com/the_darklorde">Jason VandenBerghe</Link>, designer of Ubisoft's _For Honor_, is a great primer on how psychology affects game design. 
<Link to="https://www.gamasutra.com/view/feature/129948/the_chemistry_of_game_design.php?page=1">
Written by <Link to="https://twitter.com/danctheduck">Daniel Cook</Link> about a decade before Jason's _Engines of Play_. It's an earlier look into how game designers were thinking about games in a time before app stores and the current attention economy. 
<Link to="https://tonysheng.substack.com/">
* <Link to="https://tonysheng.substack.com/p/game-economies">Do Open In-Game Economies Make Games Less Fun</Link>
* <Link to="https://tonysheng.substack.com/p/vitaliks-warlock-probably-deserved">Vitalik's Warlock Probably Deserved It</Link>
<Link to="https://steamcommunity.com/profiles/76561198024087514/recommended/8500/">
<Link to="https://www.humanetech.com/podcast/28-two-million-years-in-two-hours-a-conversation-with-yuval-noah-harari">
<Link to="https://www.deconstructoroffun.com/">
<Link to="https://mitpress.mit.edu/books/virtual-economies">
<Link to="https://medium.com/curiouserinstitute/a-game-designers-analysis-of-qanon-580972548be5">
<Link to="https://www.ted.com/talks/emily_levine_a_theory_of_everything">
<Link to="https://play.google.com/store/apps/details?id=com.pinkpointer.kakuro">
<Link to="https://play.google.com/store/apps/details?id=com.innersloth.spacemafia&hl=en&gl=US">
<Link to="https://www.wired.com/story/mirrorworld-ar-next-big-tech-platform/">
<Link to="https://www.forbes.com/sites/cathyhackl/2020/07/05/the-metaverse-is-coming--its-a-very-big-deal/?sh=1781cf2d440f">
<Link to="https://www.wired.co.uk/article/metaverse">
<Link to="https://medium.com/the-challenge/state-of-the-metaverse-2021-9f032fed655b">
<Link to="https://tonysheng.substack.com/p/the-virtual-worlds-future-is-already">
<Link to="https://gamasutra.com/">
<Link to="https://www.gamesindustry.biz/">
Please register for the Kernel Gaming Track Global Game Jam <Link to="https://globalgamejam.org/2021/jam-sites/kernel-gaming-guild-game-jam-2021">here</Link>.
<Link to="https://austingriffith.com">
<Link to="https://ronan.eth.link/">
<Link to="https://bitcoinmagazine.com/articles/how-hackathon-birthed-cryptokitties-origin-story1">
<Link to="https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md">
<Link to="https://www.wired.com/2011/07/mf-chainworld/">
> Separate the processes of creation from improving. You can’t write and edit, or sculpt and polish, or make and analyze at the same time. If you do, the editor stops the creator. While you invent, don’t select. While you sketch, don’t inspect. While you write the first draft, don’t reflect. At the start, the creator mind must be unleashed from judgement. - <Link to="https://kk.org/thetechnium/68-bits-of-unsolicited-advice/">Kevin Kelly</Link>
<Link to="https://en.gameslol.net/adventure-capitalist-1086.html">
<Link to="https://en.wikipedia.org/wiki/Video_game_crash_of_1983">
<Link to="https://www.superdataresearch.com/blog/2020-year-in-review">
<Link to="https://www.macworld.com/article/1142857/pspgo.html">
<Link to="https://www.kickstarter.com/projects/ouya/ouya-a-new-kind-of-video-game-console">
A valiant effort to disrupt the console market, a small team decides to create a new Android-based games console. It ultimately fails and <Link to="https://techcrunch.com/2019/05/22/seven-years-later-the-ouya-is-dead-for-real/">is sold to Razer</Link>, but for a time it was the symbol of an independent, open platform.
<Link to="https://www.gamesindustry.biz/articles/2016-05-11-the-death-of-toys-to-life">
<Link to="https://wtfisqf.com/">
<Link to="https://public.tableau.com/profile/paul.gadi#!/vizhome/GameIndustryTimeline/GameIndustryTimelinebasedonselectGamasutraArticles">
<Link to="https://docs.google.com/spreadsheets/d/1NwNkNDqk31cKXO5e5vm3xWs4ieUU-sgot_7hu6f4pmg/edit#gid=0">
<Link to="https://medium.com/@polats/game-industry-timeline-from-1997-2018-4b5adfa76ecc">
A curated timeline of articles from Gamasutra showing each year's relevant technologies, issues, and industry market size. Links to the articles can be found <Link to="https://docs.google.com/spreadsheets/d/1NwNkNDqk31cKXO5e5vm3xWs4ieUU-sgot_7hu6f4pmg/">here.</Link>
<Link to="https://medium.com/@polats/3-disruptive-game-design-trends-to-look-forward-to-in-2020-23c22aa4b6be">
<Link to="https://www.flashgamehistory.com/">
If we're looking to disrupt games, we also need to know the deep background. Below is a <Link to="https://medium.com/@polats/game-industry-timeline-from-1997-2018-4b5adfa76ecc">curated timeline of articles</Link> from [Gamasutra](/track-gaming/module-1/crafted/#industry-references) that gives a good overview of how the game industry evolved between 1997 and 2018. 
<Link to="https://play.google.com/store/apps/details?id=net.kairosoft.android.gamedev3en&hl=en&gl=US">
<Link to="https://ncase.me/loopy/">
<Link to="https://blog.hoard.exchange/diablo-iiis-failed-auction-house-why-true-ownership-won-t-save-your-game-c6d692b9de1">
<Link to="https://medium.com/hackernoon/building-a-cross-game-item-future-3ce16f3aea7c">
<Link to="https://hackernoon.com/how-to-build-a-robust-game-economy-lessons-from-one-of-the-worlds-longest-running-mmos-426f8fd94f6d">
<Link to="https://tonysheng.substack.com/p/crypto-adoption-games">
<Link to="https://en.wikipedia.org/wiki/Pathetic_dot_theory">
<Link to="https://www.interdependence.online/declaration/e-bw-AGkYsZFYqmAe2771A6hi9ZMIkWrkBNtHIF1hF4">
<Link to="https://www.socialtext.net/codev2/code_is_law">
<Link to="https://solidproject.org/about">
<Link to="https://opensea.io/blog/guides/non-fungible-tokens/">
<Link to="https://defold.com/open/">
<Link to="https://machinations.io/">
<Link to="https://gateway.pinata.cloud/ipfs/QmNmJcLc9Me7LERSh5shJmkgEeddFzcn4L1pTeMjT5fXqE/OV_Metaverse_OS_V5.pdf">
We can turn once again to Nicky Case, who helpfully reminds us that change usually comes from <Link to="https://blog.ncase.me/evolution-not-revolution/">Evolution, Not Revolution.</Link> 
<Link to="/images/gaming-img/openmetaverse.png">
<Link to="https://lichess.org">
<Link to="https://gamediscoverability.substack.com/">
The go-to resource for anyone who wants to understand discoverability in games. <Link to="https://www.simoncarless.com/">Simon Carless'</Link> substack is a must-follow for anyone interested in the game industry.
<Link to="https://www.gdcvault.com/play/1015595/The-5-Domains-of-Play">
An earlier talk by <Link to="https://twitter.com/the_darklorde">Jason VandenBerghe</Link> from back in 2012, done before [Engines of Play](/track-gaming/module-0/crafted/#engines-of-play). An effective framework to understand why people play certain types of games based on the Big 5 (OCEAN) psychological model.
<Link to="https://gitcoin.co/wiki/grants/">
<Link to="https://our.status.im/discover-a-brave-new-curve/">
Written by our beloved cryptowanderer, an explanation of the mathemagical approach used by <Link to="https://dap.ps/">dap.ps</Link> to improve dapp discovery. 
<Link to="https://gov.rarible.com/t/proposal-idea-temporarily-boost-feature-nfts-with-rari/656">
<Link to="https://www.w3.org/community/games/">
<Link to="https://github.com/tcmg/open-mini-games">
<Link to="https://steamspy.com/">
<Link to="https://store.steampowered.com/labs/">
<Link to="https://trello.com/b/GXLc34hk/epic-games-store-roadmap">
<Link to="https://www.twitch.tv/directory?sort=VIEWER_COUNT">
<Link to="https://www.reddit.com/r/IndieGaming/">
<Link to="https://itch.io/devlogs">
<Link to="https://steamcommunity.com/games/593110/announcements">
<Link to="https://sensortower.com/blog">
<Link to="https://gamedevjs.com/">
<Link to="https://store.steampowered.com/app/892970/Valheim/">
<Link to="https://books.google.com.ph/books/about/Finite_and_Infinite_Games.html?id=ObLBJ_w2ZlcC&printsec=frontcover&source=kp_read_button&redir_esc=y#v=onepage&q&f=false">
<Link to="https://ethereum.org/en/eips/">
<Link to="https://webmonetization.org/">
<Link to="https://www.grantfortheweb.org/">
<Link to="https://fs.blog/2020/02/finite-and-infinite-games/">
<Link to="https://stratechery.com/">
<Link to="https://store.steampowered.com/app/1070710/Kind_Words_lo_fi_chill_beats_to_write_to/">
<Link to="https://medium.com/swlh/re-fungible-tokens-in-collectible-card-games-73a61703226f">
<Link to="https://ecorner.stanford.edu/in-brief/the-knowledge-economy-of-world-of-warcraft/">
<Link to="https://www.youtube.com/watch?v=BoYyP7J5Sh0">
<Link to="https://blog.chain.link/44-ways-to-enhance-your-smart-contract-with-chainlink/">
<Link to="https://asherv.com/threes/threemails/">
<Link to="https://www.fehrsam.xyz/blog/creators-communities-crypto">
<Link to="https://tonysheng.substack.com/p/yfi-ponzinomics">
<Link to="https://medium.com/@simondlr/what-is-harberger-tax-where-does-the-blockchain-fit-in-1329046922c6">
<Link to="https://www.youtube.com/watch?v=bHM1DRHSUPw">
<Link to="https://blog.zkga.me/announcing-darkforest">
<Link to="https://feedvitalik.com/">
<Link to="https://www.forbes.com/sites/davidthier/2020/08/13/freefortnite-watch-epic-games-anti-apple-198fortnite-commercial-here/">
<Link to="https://www.theverge.com/2020/8/13/21368363/epic-google-fortnite-lawsuit-antitrust-app-play-store-apple-removal">
<Link to="https://www.projecthorseshoe.com/reports/featured/ph19r7.htm">
<Link to="https://www.gamasutra.com/view/feature/1524/the_chemistry_of_game_design.php?print=1">
<Link to="https://www.gdcvault.com/play/1023329/Engines-of-Play-How-Player">
<Link to="https://www.deconstructoroffun.com/">
<Link to="https://lostgarden.home.blog/">
<Link to="https://gamediscoverability.substack.com/">
<Link to="https://docs.google.com/presentation/d/1adZXo4RVS2A4CtrU-8gJhV4SDn3Z26jDliJIPlUHwic/edit?usp=sharing">
<Link to="https://blog.chain.link/create-dynamic-nfts-using-chainlink-oracles/">
<Link to="https://chain.link/presentations/devcon5.pdf">
<Link to="https://medium.com/genie-platform/meet-the-genie-6bb60fe67943">
<Link to="https://medium.com/@billyrennekamp/re-fungible-token-rft-297003592769">
<Link to="https://books.google.com.ph/books/about/The_Grasshopper.html?id=G9z4wjVB_0wC&redir_esc=y">
<Link to="https://dashboard.sarafu.network/">
<Link to="https://gitlab.com/grassrootseconomics/cic-modeling">
<Link to="https://medium.com/@austin_48503/nifty-ink-an-ethereum-tutorial-c860a4904cb2">

Process finished with exit code 0
