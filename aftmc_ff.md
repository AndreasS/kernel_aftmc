[[_TOC_]]
# AwakeningFromTheMEaningCrisisBackup Future Thinkers from Google Docs:

Awakening From The Meaning Crisis
Lecture series by John Vervaeke
Notes by Euvie Ivanova (with additions & comments by others)

Taken live during Future Thinkers watch parties

Join us every Monday at 9am PST / 5pm GMT
at https://www.youtube.com/futurethinkers


Ep 18 - Awakening From The Meaning Crisis - Plotinus and Neoplatonism
Ep 17 - Awakening From The Meaning Crisis - Gnosis and Existential Inertia
Ep 16 - Awakening From The Meaning Crisis - Christianity and Agape
Ep 15 - Awakening From The Meaning Crisis - Marcus Aurelius and Jesus
Ep 14 - Awakening From The Meaning Crisis - Epicureans, Cynics and Stoics
Ep 13 - Awakening From The Meaning Crisis - Buddhism and Parasitic Processing
Ep 12 - Awakening From The Meaning Crisis - Higher States of Consciousness Part 2
Ep 11 - Awakening From The meaning Crisis - Higher States of Consciousness
Ep. 10 - Awakening from the Meaning Crisis - Consciousness
Ep. 9 - Awakening from the Meaning Crisis - Insight
Ep. 8 - Awakening from the Meaning Crisis - The Buddha and "Mindfulness"
Ep. 7 - Awakening from the Meaning Crisis - Aristotle's World View and Erich Fromm
Ep. 6 - Awakening from the Meaning Crisis - Aristotle, Kant, and Evolution
Ep. 5 - Awakening from the Meaning Crisis - Plato and the Cave
Ep. 4 - Awakening from the Meaning Crisis - Socrates and the Quest for Wisdom
Ep. 3 - Awakening from the Meaning Crisis - Continuous Cosmos and Modern World Grammar
Ep. 2 - Awakening from the Meaning Crisis - Flow, Metaphor, and the Axial Revolution
Ep. 1 - Awakening from the Meaning Crisis - Introduction by John Vervaeke

# Ep 19 - Awakening From The Meaning Crisis - Augustine and Aquinas 

The triangle of Christianity - Neoplatanism - Gnosticism 

Augustine lived during the time of Roman empire decline, and was attracted to darker idea, such as the nature of evil and personal conflict. Augustine was a sex addict. In his philosophical explorations, he seeks relief from his own demons, as well as the world’s demons. 

He has a mystical experience, but can’t stay there, which brings him despair. 

He then discovers the work of Paul (early version of the Bible), and the inner conflict described in it makes sense to him. He is also majorly influenced by the work of Plato and Plotinus. 

They believed that at the core of everything was a love for the good, the true, and the beautiful 

Augustine believed that his sex addiction was caused by his inability to love property. He thought that participating in Agape (selfless love) would heal him.

Therefore, he believed that Neoplatonism needs Christianity and Gnosis. 

His philosophy was an existential manual because it was deeply personal. 

The process of moving from the less real to the more real, and becoming what is more real. 

3 things that contribute to meaning in life:

Coherence (the real, nomological order, things make sense)
Significance (the good, normative order, things matter, agape)
Purpose (the beautiful?, narrative order, things have a story)

Augustine’s thinking lays the foundation for the Medieval worldview. 

It is simultaneously a scientific, spiritual, therapeutic, and existential philosophy, which ends up lasting 1000 years. 

The Roman empire collapses, and causes a traumatic loss of cities, literacy, trade. The standard of living in Rome is not recovered again until 1750 (in England). 

In 1054, there is a division between the Western and Eastern Christianity. In this schizm, Western Christianity loses its connection to the Neoplatonic mystical theology. It becomes more Aristotilian. 

There is a shift in how people read, from reading aloud and communally (i.e. reciting the Bible) to reading silently to themselves. 

A new model for thought emerges. Old model: thought conforming to the world (participatory knowing), a way of being / becoming / knowing that is all one, New model: thought is to have coherent propositional knowing, language, beliefs. 

Reading stops being a spiritually transformative psychotechnology.

Aquinas 

Two worlds:
Natural world - can be studied by science and reason
Supernatural world - can only be accessible by faith and love

Aquinas believed that love moves the will to assert things it cannot know through reason. Faith becomes the act of wilful assertion driven by the love of God. 

As our will becomes driven less to love and more by reason, the supernatural world becomes less and less real. The whole heritage of the Axial revolution starts to fall apart. 

# Ep 18 - Awakening From The Meaning Crisis - Plotinus and Neoplatonism 

The cognitive science of gnosis

People being existentially stuck can be very damaging and tear apart your agency. Gnosticism was a way to break out of that. 

What is needed is a rediscovery of serious play through liminal states in which people can enact the self and world they want to live with.

There are also dangers to these kinds of transformative practices - people can fall prey to bullshit and self-deception. Having a supportive community, a wisdom tradition, and an ecology of practices are ways to safeguard against this. 

Many historical gnostics saw themselves as Christians. For them, it was less important to believe in Jesus, and more important to become like Jesus and experience a oneness with God. They saw Jesus as a teacher who demonstrated an experiential knowing, much like a shaman. 

Demiurge - a rational agent who shapes things in space and time. 

Gnostics saw the everyday world as full of suffering and entrapment, so they concluded that whoever created this world must be stupid and evil. They believed that social and cultural structures were thwarting them (this is similar to how many people feel today). They identified all existing gods - Jewish, Greek, and Roman, and saw these gods as the guards of their prison, and must therefore be transcended. They believed there was a divine spark within each person that could carry them to the level of gods (into the experience of Agape). This was a very radical idea at the time. Before then, the relationship between people and gods was that of servitude.  

Gnostics were famous for thinking that women are as equally capable of a spiritual life as men. This was also a radical idea at the time.

They didn’t see the purpose of Christianity as a dogma, but as a mythology that can help us free ourselves from existential suffering. 

This is why it was like an axial revolution within an axial revolution, it turned the mythology of the ancient world on its head. 

The Matrix has a deeply gnostic story. So is The Truman Show. Star Wars also has gnostic undertones. 

The dark side of the gnostic vision is that it’s a grand conspiracy theory. “Behind all the apparent chaos and suffering, there is some evil overlord.” Nazi-ism is a twisted form of gnosticism. 

Many people today are seeking gnosis to find relief from the meaning crisis. If we are to make use of gnosis, we have to separate it from gnosticism. It’s important to have an ambivalent attitude towards it. 

Jung transformed the gnostic mythology into psychotherapy. It was a way to give us a scaffolding for enacting anagoge.

Christianity - Gnosticism - Neoplatonism, an important triangle for understanding the meaning crisis. Neo in the Matrix is a nod to Neoplatonism, The One is an important idea in both. 

Plotinus created the unified field theory of the ancient Western spirituality.

The scale between pure potentiality and pure actuality

Realness as a function of the level of integration of the structural functional organization

The more integrated you become, the more real, and the clearer you can see reality.


# Ep 17 - Awakening From The Meaning Crisis - Gnosis and Existential Inertia

Gnosticism - a branch of Christianity who held Gnosis as a central practice
The Axial revolution within the Axial revolution. 

Gnosis - the process that allows us to overcome being existentially stuck by enacting transformation through ritual 

The worldview of reality as an arena that progressively reveals itself to you 

Spinoza had a particular worldview that is available to be embodied. 

Transframing - reformation of both the agent and arena; not only a reframing of the problem but also a transformation of the person experiencing it  

The opposite is an inability to enter into a different viable way of being, which can be either positive or negative. 

Existential inertia - being stuck in a way of being, even if you may not want to be there.

People often enter therapy for this reason, being stuck.

Transformative experiences get us to confront a fundamental ignorance, which helps us get unstuck. You have to go through the experience before you can know what it’s like to have the experience. There is irreversible change that we cannot understand until we go through it.

Games that allow us to “bleed” into them, where the lines between our real life and the game become blurred

Enactive analogy - an analogy you enact with your actions, which takes skill. 

Recovering serious play and ritual, putting us in a liminal space between the normal world and the sacred world. This can allow us to test how things feel before we go through them in real life. This is how we can get out of being existentially trapped. 

# Ep 16 - Awakening From The Meaning Crisis - Christianity and Agape

Jews had the psychotechnology as understanding history as a narrative

Jesus saw himself as deeply participating in history rather than being passive in it, he felt himself being at one with God. God is that which creates the world, and Jesos saw himself as co-creating the world with God. 

Participatory knowing - reciprocal revelation where you are making your life / the world and it’s making you 

Different kinds of love

Eros - seeking to be one with something. Sexual, spiritual (mystic union with God), or physical (being one with a cookie by eating it). Consummation. 
Philia - reciprocity. Friendship. 
Agape - giving and unconditional caring. Parenthood. It turns a non-person into a person. 

Agape - by participating through love in another being’s life, we are radically transformed. This is the transformation a parent goes through when they have a child.

Metanoia - a spiritual transformation. We turn from being egocentric to being centered on someone else. 

We gain a sense of self and the ability to reflect on our sense of self through other people. 

We are all born out of an agapic love that precedes us. This is how we went from being a non-person to a person, by consuming the love of our parents. 

The agape that preceded me flows through me and transforms me as I give it. We become vessels through which agape creates other human beings. This is a God-like creative process. We are not agape, we participate in it. 

Agape has a sacrificial element, giving before the person has earned it. 

A central idea that Jesus presented is the importance of forgiveness, because it affords agape. For-give - give before someone has earned it. 

Jesus’ death exemplifies the sacrificial forgiveness that affords agape, it has become a symbol of it. 

Saul - one of the early Jesus prosecutors. He was a Roman and a Jew, and he integrated these two parts of his identity around the importance of laws. He is involved in prosecuting the early Christians, the followers of Christ (which means The Anointed One).

Saul has a transformative spiritual experience, where he is struck to the ground and blinded by a bright light, and has a vision of Jesus speaking to him. He has intense cognitive dissonance; he is torn between his vision and his previous identity. He is then taken in by the people he was persecuting, and nursed back to health (given agape). 

He then goes into the desert to reflect, and comes back changed. He changes his name to Paul and starts spreading a new message.

“Love is patient, love is kind” (speaking of agape, not romantic love)

Gnosis - knowing through transforming yourself into something 

There is a danger in this, because we project our own unconscious onto that which we love. The more unconscious, the darker this can be.

This is what happened to Paul. His inner conflict was major. He projected his inner conflict onto the world and onto God. He thinks that God is conflicted in himself: one part represents law and order, judgement. The other part is all-loving. 

People who feel like they are not living up to the fullness of their personhood become deeply attracted to this central message in Christianity.

There is a price to pay for the Grace of transformation in the Christian sense.



# Ep 15 - Awakening From The Meaning Crisis - Marcus Aurelius and Jesus

Epicureans - philosophical group of this era concerned with alleviating anxiety and suffering by setting our heart on things that matter.

Cynics - a philosophical group concerned with differentiation between moral code and social purity code.

Stoics - a philosophical group interested in understanding how our identity is being forged and deconstructing it.

MAR - Mindless Automatic Reaction 
Proseche - paying attention to paying attention. Mindfulness.
Proseichon - ready to hand. Remembering in the sense of satí (remembering in a modally existential sense. ) Practice of psycho-technologies.

They practiced paying attention to paying attention. There is a difference between the meaning (existential not semantic meaning) and the event. This is still at the core of modern psychotherapy. 

You don’t have as much control over life as you think you do. The core of wisdom is knowing what is and isn't in your control. 

Having mode (needs that are meant by controlling circumstances) vs. being mode (needs that are meant by enhancing meaning, becoming more mature). We get confused when we mix up the two - i.e. when we try to gain a sense of maturity by purchasing a car or house, or gain love by trying to control another person. 

Practicing psycho-technologies moment to moment 

Meditations by Marcus Aurelius is a book that is often misunderstood. It is not meant to instill beliefs in you. He wrote the books for himself in order to bring those practices into his awareness. 

Marcus Aurelius was arguably the greatest Roman Emperor. He is one of the most known stoic philosophers. 

He faced the challenge not to get enmeshed in power and fame. 

“You can become happy even in a palace” -> similar to the Buddha’s path

He practiced separating the event or object from the meaning associated with it.

We have control of the meaning we are making of something, but little control over external events.

Ubiquitous evil is the most dangerous - i.e. romantic comedies. Romantic comedies conflate the events with the meaning, which is the opposite of stoic practice. 

It’s not mortality that make us anxious, it’s fatality. 

Death is the ultimate loss of agency and your ability to make meaning.

Most therapy works by cognitively reframing the meaning of an event (existential mode) instead of thinking about how we could or could have changed an event. It can make us be Socratic with ourselves - questioning our beliefs and thoughts. This helps decrease our self deception.

One of the keys to wisdom is properly identifying our sense of control and altering our sense of identity. This helps solve our anxiety about mortality. 

List all the things you wish you could have done if you lived forever. Once you realize that this list is finite, it helps accept mortality. We don’t really want the length of life, but a depth and quality. We want a fullness of being.  

There are answers to the quest for meaning, wisdom, and self-transcendence in Western traditions, not only Eastern (i.e. Buddhism). There has been a resurgence in the interest in Plato, Socrates, Aristotle, and the stoics. 

Axial revolution in Ancient Israel.

Kairos (participatory time, being in the right moment) vs. Chronos (clock time)

God’s creative Logos (the word he speaks through the prophets, the word that creates history, that causes Kairos). The underlying structure of culture.

Kairos though Logos

Jesus - the myth of being born again is referring to a spiritual awakening, a radical transformation. He incarnates the principle by which you can intervene in your own personal history so that you can have a new mode of existence. 

Love isn’t an emotion or a feeling, it’s a mode of being. It can express itself in many emotions, feelings, behaviours, etc. Loving a person romantically is not the same as loving tennis. 

Three kinds of love:

Eros - seeking to be one with something. Sexual, spiritual (mystic union with God), or physical (being one with a cookie by eating it). Consummation. 
Philia - reciprocity. Friendship. 
Agape - giving and unconditional caring. Parenthood. It turns a non-person into a person. 

# Ep 14 - Awakening From The Meaning Crisis - Epicureans, Cynics and Stoics

After the Axial Revolution

Alexander The Great - conquers and spreads the Greek way of thinking across the Mediterranean. He represents the pre-Axial way of being. The period he lived in is known as the Hellenistic era. 

Greece is divided into many Polis, or city states. Very tightly knit communities for many generations, where people’s identities were completely immersed in their Polis. 

Alexander came and destroyed these. People experienced a radical sense of domicide, a destruction of home, where they no longer felt at home where they were. An era of great anxiety.

People try to merge different religions together to cope with this anxiety, with an elevation of mother goddesses, like Isis. 

Epicurus - philosopher of this era concerned with alleviating anxiety and suffering, not just elevating wisdom like Socrates or Aristotle.

A new model of philosopher arises: someone who can cure you of existential suffering, a physician of the soul. 

Epicureans diagnose that the main problem is fear or anxiety. 

Fear is a response to an observable direct threat.
Anxiety is when the threat is nebulous. This is more common with existential issues. 

Evidence that your consciousness is emergent from your brain is overwhelming. 

Instead of trying to achieve immortality, we can radically accept our mortality.

What we’re afraid of is the loss of agency that comes with approaching death, of losing what’s good, the things that give us meaning. 

Epicureans believed that meaningful relationships are very important, and included women in their community - not just for sexual partnerships, but for friendship and practicing philosophia (the pursuit of wisdom). 

Epicurean method for dealing with anxiety - learning to live with the acceptance of death. 

Stoicism - direct and explicit ancestor to current forms of psychotherapy, like Cognitive Behavioral Therapy. 

Stoics have a different diagnosis of the problem. 

Antisthenes - philosopher who developed a practice for talking to yourself using the Socratic method

Diogenes - philosopher who was immortalized in the Tarot card of a man with a lamp (he once walked into the market with a lamp asking if there was even one honest man there). He was very provocative in his ways of making people question their assumptions, what they take for granted. He famously lived “like a dog” in a barrel outside of the city, and masturbated in public at the market to get a reaction from people. 

The Cynics - group of philosophers after the Axial era. Their understanding of the Hellenistic anxiety was that we are afraid to lose the things we set our heart on (similar to Buddhist view that attachment creates suffering). They believed that we should learn how to set our hearts not on the things that are man made (like institutions), but on the laws of the natural world and moral laws. 

Guilt (violating personal moral code) vs. shame (violating social purity code, highly related to power structures within the culture)

What the Cynics got wrong is that they focused on the product (what to attach to or identify with) instead of the process (of attachment and identification).

The Stoics noted that it is the process of assuming and assigning identity that needs focus.

# Ep 13 - Awakening From The Meaning Crisis - Buddhism and Parasitic Processing    

Transformational states optimize our core mental processing

When the Buddha awakens, it brings about a deep remembering of the being mode, analogous to a developmental shift

There have been many misunderstandings in how Buddhism is viewed in the West

Stephen Bachelor in his books says we are confronted with 2 different positions:

1. You can only interpret Buddhism from within the tradition. It’s not about altering your beliefs, it’s about going through a personal transformation by doing specific practices. Seeks transformative relevance.  

This view can be myopic, as there are many branches of Buddhism.

2. You can view Buddhism from the outside. This is the academic view that seeks an objective view, and does not seek to engage in any practices. Seeks the objective truth.

S. Bachelor suggests that we need both of these methods of studying Buddhism. It is necessary to overcome our fixation on beliefs (which is a post-Christian way of thinking). 

The meaning-making machinery does not occur at the level  of beliefs. 

We can look at Buddhism existentially. 

At the core of Buddhism are the 4 Noble Truths. These are not beliefs; they are the things that can help afford the kind of transformation that the Buddha went through. They can help us re-enact the Buddha’s enlightenment in ourselves. They provoke people into change. 

All of life is suffering.

The original meaning of suffering is “to undergo”, to lose agency. This is not about pain, it’s about losing your freedom. 

All of life is threatened by the possibility of losing your freedom (or losing your sovereignty). Every time you exercise intelligent agency, you put yourself at risk of self-deception. 

The same mental machinery that helps us become adaptive to the world and have insight, is the same machinery we use for self-deception. 

We use heuristics, or mental shortcuts, to make sense of reality quickly. They are adaptive, but they also distort reality. Our mind can create feedback loops that reinforces our self-deceptions and addictions.

This is parasitic processing.  

The standard model of addiction is wrong. It is not about a biochemical dependency or compulsion. You can get addicted to non-biochemical processes. There are many examples of people spontaneously stopping their addiction. 

A different model of addiction is reciprocal narrowing, which is a feedback loop that constrains our agent-arena relationship to where it feels like we have no  alternatives available to us. Breaking frame can help us get out of this loop. 



The cessation of suffering is available.

The same machinery that we used to get into self-deception can be used to get us out of it. It can be a downward or upward spiral. 

This is done through practices. We can cultivate a dynamical system that intervenes in our mental processes in a systematic way, at the level of our states of consciousness and traits of character. 

The 8 fold path. 

Right Understanding
Right Thinking
Right Speech 
Right Action
Right Livelihood
Right Mindfulness
Right Concentration
Right Samadhi

This means getting the optimal grip.

These should make you feel both threatened and encouraged - that’s what provokes change. 


# Ep 12 - Awakening From The Meaning Crisis - Higher States of Consciousness Part 2

Higher states of consciousness

Understanding enlightenment from a cognitive science perspective rather than mystical perspective

Different experience of self - sense of oneness with everything, flow, egolessness, sense of self that is in participation with the external world rather than separate from it

Continuity hypothesis:

Fluency -> Insight -> Flow -> Mystical experiences -> Transformative experiences -> Awakening)

Part of this process is increasing variation in cognitive processes through disruptive strategies in order to reveal invariance (that which remains the same). 

This is like a child going through developmental stages of seeing systematic errors in their cognition 

Decentering strategies

The Solomon effect 

People tend to get stuck in their own egocentric perspective. By decentering or breaking frame, they are able to see things from a different perspective and often resolve the problem.

Awakening is having a systematic transformation in cognition, which can be a powerfully traumatic and terrifying experience.

Pursuing this in an autodidactic fashion (isolated, by yourself) is very dangerous because a person can end up in endless egoic loops. North American individualism can over-glorify this unnecessarily. 

This is why it is recommended to undergo this process within a wisdom tradition or community that can provide support, guidance, and perspective. The Buddha said that the Sangha, or community, is necessary for this transformation of the self. 

This is a process where you and the world are reciprocally revealing themselves.

The self is a systematic set of functions for integrating things in the world together. By making things relevant to ourselves, we can then make those things relevant to each other 

In a transformative awakening experience, all the energy bound up in our egocentric processes is redirected onto the world. This is why during these experiences, the world looks so luminescent and beautiful.

At the information processing level in machine learning, similar disruptive strategies are used. Noise and randomness is introduced into the network, and it’s essential to the self optimization of the algorithm. Algorithms are very good at picking up on patterns (as is the human mind), so it can often pick up on false patterns (called overfitting).

Noise and randomness helps reduce the incidence of picking up on false patterns. We want the algorithm to find a function that doesn't just describe the data sample, but also generalizes outside of the data set. 

It’s the same reason why pursuing transformation within a wisdom tradition or community allows you to understand the experience in a way that generalizes to other people’s experience and not get stuck in egoic loops.

The system that is the human mind both organizes itself and breaks itself, oscillating back and forth between these functions in order to achieve higher order complexity 

The brain is either integrating or segregating things. It complexifies by going back and forth between these modes. On psilocybin, the brain does both at the same time, which is called metastability. This is why psilocybin often produces transformative experiences.

Plausibility

Probable (this is not how we’re using it)
Makes good sense, stands to good reason (this is how we’re using it)

Plausibility is not certainty. Both in our mental processes and in science, we rely on plausibility, not certainty. Both are continuously self-amending. We’re trying to find the best explanation. This is how we determine what’s “real”. Reality is never certain, it’s only the most probable explanation based on current understanding. 

Trustworthiness - Reproduced by independent but converging sources. If you’re only seeing something, and you’re the only one seeing it, it’s likely an illusion. If we can confirm something with multiple senses, and with multiple people, it decreases the likelihood that it’s an illusion. 

This is why science favours numbers. Quantification increases the trustworthiness of information.


Elegance - If we can use the same model in many different places, we call it elegant.

Elegance, Fluency, and Convergence need to be in balance.

The converging inputs and the outgoing applications need to be balanced. 

This is how the brain evaluates the plausibility of its processes while in an altered state. 

Altered states of consciousness are merely useful tools. 

The information (propositional knowing) gained in these states is useless; it’s about the practice of increasing wisdom (participatory knowing).


Recommended books from the discussion
Waking from sleep by Steve Taylor

# Ep 11 - Awakening From The meaning Crisis - Higher States of Consciousness   

Higher states of consciousness
We judge realness by measuring its coherence with our everyday experience. However, in altered states, this is not the case. In these states, people feel a deep connectedness to reality, but can’t explain how or why. 

3 Factors for understanding & describing higher states of consciousness: 

How is the world being experienced?

Bright, shining, glorious, vivid, intense, clarity, an expansion of vision - both broader and more detailed. “The world in a grain of sand”. The world is more intricate, interesting, alive, significant, and beautiful. An increased feeling of making sense of things. All of this comes together in a deep sense of oneness or integration. 

How is the self being experienced?

Profound sense of peace, joy. People often report a change in the sense of self - disappearance of autobiographical sense of self (similar to states of flow), a remembering of a “true” sense of self. Sense of energy and vitality. Sense of insight and understanding. 

How is the relationship between the sense of self and world being experienced? 

Deep and profound connection and oneness. A sense of truly participating with the reality, sharing of identity between self and world. Deep participatory knowing. Many describe this experience as ineffable. 

These states are often preceded by disruptive strategies. Some are long term, some are short term.

Mindfulness practices - meditation, contemplation 
Fasting
Sexual deprivation
Sleep deprivation
Drumming
Chanting
Psychedelics

People who were practicing mindfulness and then took psychedelics had more profound experiences. 

These practices help to set up insight. You have to break frame before making a new frame. 

These experiences had a measurable improvement in people’s lives, including a reduction in the fear of death. 

Decentering - shifting from an egocentric (first person) to an allocentric (third person) experience of moving through the world. 

Supersalience - the salience of reality eclipses the narcissistic glow of our own ego. This produces a reduction in self-deception, a reduction in bullshitting. 

The act of making sense, of finding coherence, actually makes people report their experience of life as more meaningful.

Enhanced meaning + enhanced understanding + self-guidance

Insight is a fluency spike. An increase in how fluently we process information, so we judge it as more real. 

When information is better organized and easier to understand, we rate it as more true, even if the content is the same. 

Alphabetic literacy did exactly this. It made the information seem more real by making it more clear. 

In the flow state, we start to get the precursors of a mystical experience - supersalience, egolessness, oneness, universality, etc

Continuity hypothesis:

Fluency -> Insight -> Flow -> Mystical experiences -> Transformative experiences (i.e. Awakening)

The same machinery is being exapted (repurposed) to produce more and more powerful experiences. It’s a process of priming. The more we practice this feedback cycle, the more likely it is to produce awakening type experiences. 

Expertise helps us get into flow. The kind of expertise we use in higher states of consciousness is the expertise of making sense of the world. 

Optimal grip - when observing something, finding the perfect trade-off relationship between seeing as many details as possible while also seeing the gestalt (the overall thing). It’s a dynamic balance. As comprehensive yet as detailed as possible (these are in a trade-off relationship). Our perception is always doing this, but we don’t realize it because we learn how to do this as children. 

The basic level - a category of object that is optimally relevant to the situation, that has optimal grip. I.e. “Look at that dog” (rather than mammal or cocker spaniel).

We increase this skill further by engaging in mindfulness practices. When we consistently practice getting an optimal grip on reality (our perception of the world, ourselves, and the relationship between the two), it leads to awakening experiences. 

Moderate amounts of distraction disrupt our framing, which helps us make new frame and can lead to insight. Going for a walk, taking a break, sleeping on an idea, etc. The disruptive machinery is exapted to bring about a higher state of consciousness. They increase the variation of cognitive processing. 

De-automatization of cognition. Our automatic framing blocks us from solving problems or seeing things differently that we are used to. De-automatizing our cognition helps us see things outside of the box. 

Invariance?

In an awakening experience, you realize and zoom in on the systematic errors in your own cognition, which is related to why it is so transformational. 

References 

May not be exactly what John talked about. The names of the people may be more accurate. Times are for the original video.

08:20 https://youtu.be/8BhOkP5bsDM Lecture by Honorary Doctor Elaine Scarry: The Beauty of the World and the Nuclear Peril, Elaine Scarry, author of - On Beauty.   https://philosophynow.org/issues/44/On_Beauty_and_Being_Just_by_Elaine_Scarry 

17:10 David Bryce Yaden, University of Pennsylvania   Department of Psychology. Haidt, Jonathan - Hood, Ralph - Vago, David  - Newberg, Andrew  - 2017/05/01 - The Varieties of Self-Transcendent Experience  https://www.researchgate.net/profile/David_Yaden2/publication/316611489_The_Varieties_of_Self-Transcendent_Experience/links/591311afa6fdcc963e7ecd39/The-Varieties-of-Self-Transcendent-Experience.pdf 

31:44 http://www.andrewnewberg.com/books/how-enlightenment-changes-your-brain Andrew Newberg and Mark Robert Waldman - How Enlightenment Changes Your Brain: The New Science of Transformation 

34:02 https://plato.stanford.edu/entries/merleau-ponty Maurice Merleau-Ponty - Phenomenology of Perception 

34:20 https://ndpr.nd.edu/news/retrieving-realism/ Hubert Dreyfus and Charles Taylor - Retrieving Realism 

46:30 https://philpapers.org/rec/IRVMIU Mind-wandering is unguided attention: accounting for the “purposeful” wanderer, Zachary C. Irving...

52:45 Cognitive Psychology
Volume 22, Issue 3, July 1990, Pages 374-419
Cognitive Psychology, In search of insight, Craig A Kaplan, Herbert A Simon https://www.sciencedirect.com/science/article/pii/001002859090008R?via%3Dihub


# Ep. 10 - Awakening from the Meaning Crisis - Consciousness 

Hard problem of consciousness is the problem of explaining how and why sentient organisms have qualia or phenomenal experiences—how and why it is that some internal states are felt states, such as heat or pain, rather than unfelt states, as in a thermostat or a toaster (from Wikipedia).

What is consciousness? How does it emerge from the brain (and does it)?
What does consciousness do? 

We know we are conscious just by being conscious. 

We have no conscious awareness of what our brain is doing.

We cannot make use of all the available information at the same time and all the ways it could be connected. Consciousness helps us zero in on the relevant information, put it together in a relevant way, and use it. This is why it’s so tightly associated with working memory and intelligence. 

A prominent theory of consciousness is integrated information theory (proposed by neuroscientist Giulio Tononi in 2004). He proposes a Turing test for consciousness. The theory predicts whether that system is conscious, to what degree it is conscious, and what particular experience it is having. 

Consciousness seems to be a way to coordinate attention to optimize how insightfully you can make sense of the world. 

Altered states of consciousness alter what is relevant and salient to us. 

Consciousness creates a salience landscape. Framing -> Figuration -> Foregrounding -> Featurization (?). The feedback loop works in both directions and between levels as well. 

Affordance: https://www.edge.org/response-detail/27002
Psychologist James J. Gibson introduced the term affordance way back in the seventies. The basic idea is that the perceptual systems of any organism are designed to “pick up” the information that is relevant to its survival and ignore the rest. 

Consciousness helps us get an optimal grip on what is happening and determine the difference between correlational and causal patterns. It helps us get into deeper contact with the world. 

When we transform consciousness, we transform the whole complex machinery of it. An awakening is not an insight IN consciousness, but an insight OF consciousness. It’s not just a flash of insight, it’s a complete overhaul. It changes the salience landscape.

An awakening is like the transformation from a child to an adult. 

In a child’s cognition, the errors are not random but systematic. It’s the same with the adult’s cognition. There are certain kinds of systematic illusions that the child is prone to that the adult isn’t, and that the adult is prone to than an awakened one isn’t. 

Salience -> Presence -> Depth

Most altered states of consciousness don’t actually produce awakening, but more illusion and self-deception. An awakening produces a sense of “more real than real”, both when it comes to the agent (understanding of self) and arena (understanding of the world). 

Awakening experiences are usually devoid of content (they do not provide any special knowledge or evidence), they are transrational and ineffable, yet they tend to trigger a major drive to transform one’s life because they challenge one’s sense of what is real and reveal the illusions one used to hold.

Realness - the pattern of intelligibility with the widest scope.

We need both a descriptive account of awakening (what is happening) and a prescriptive account of awakening (is it justifiable and how is it useful), and those two need to be coherent with each other.


Relevant resources: 
Discussed in the episode:
03:25 Professor David Chalmers: "The Meta-Problem of Consciousness" | Talks at Google https://www.youtube.com/watch?v=OsYUWtLQBS0 
08:59 Bernard Baars - A Cognitive Theory of Consciousness
 https://www.sscnet.ucla.edu/comm/steen/cogweb/Abstracts/Baars_88.html 
22:07 James Gibson - The Ecological Approach to Visual Perception
 https://www.jstor.org/stable/429816?origin=crossref&seq=1 
17:27 Wallace Matson - Sentience 
 https://academic.oup.com/pq/article-abstract/28/113/358/1520640?redirectedFrom=fulltext 
40:16 Andrew Newberg and Mark Robert Waldman - How Enlightenment Changes Your Brain: The New Science of Transformation
 https://www.npr.org/2010/12/15/132078267/neurotheology-where-religion-and-science-collide 
 https://youtu.be/njGjLQBrwZ4 
40:01 Steve Taylor - Waking From Sleep: Why Awakening Experiences Occur and How to Make Them Permanent
 https://youtu.be/xJ0ADV8Y3LI 
13:21 Giulio Tononi - Phi: A Voyage from the Brain to the Soul
 https://youtu.be/SqI1NP2_U70 
09:16 Philip David Zelazo, Morris Moscovitch, and Evan Thompson (Editors) - The Cambridge Handbook of Consciousness  https://www.google.com/books/edition/The_Cambridge_Handbook_of_Consciousness/o9ZRc6-FDg8C?hl=en&gbpv=1&printsec=frontcover 

# Ep. 9 - Awakening from the Meaning Crisis - Insight 

Attention is an optimization strategy, not a spotlight. It’s a cognitive unison where many mental processes share a goal. 

How can mindfulness train attention as to cause more insight, and eventually a systematic train of insights that leads to transformation? 

Exercise: Transparency to opacity shift, or from subsidiary (implicit) awareness to focal (explicit) awareness. 

Attention is a structuring phenomena, from implicit awareness through explicit awareness. There are many layers of structuring to it, which we are usually not conscious of. 

Looking at vs. looking through - which part are we indwelling in? I.e. looking through the glasses, we don’t think of the glasses as separate. Touching an object with a pen, we experience the pen as part of us. 

The attention is also constantly shifting from the features (parts) to the gestalt (whole).

It’s not about where the attention is “resting”, it’s about the direction it’s going into - towards explicit or implicit awareness, or towards features or the gestalt. Attention operates in a dynamic fashion, it’s always changing and optimizing itself. 

When we step back and look at the functioning of our own mind, we break up the gestalt into the features. 

Contemplation (i.e. Metta): Scaling up, widening the field of awareness - towards gestalt, towards transparency

Meditation (i.e. Vipassana): Scaling down, narrowing the field of awareness - towards features, towards opacity

There are two moments to having an insight:
Break up the gestalt (incorrect framing)
De-automatize cognition (bring it from unconsciousness into consciousness)

Bringing things into awareness: making them opaque. Looking at them rather than through them. 

To bring about insight, we need to train both the narrowing and the widening of awareness.

The strings need to be not too tight, not too loose. We need to be constantly finding the right balance between narrowing and widening of awareness, or scaling down and scaling up. This is what mindfulness does. 

By scaling down (in meditation), we make the mind less representational. Looking at the mind rather than looking through it. Stepping back and back. 

Doing this a lot can bring about a certain type of mystical experience: 

The pure consciousness event. You are not conscious of anything in particular, you are just conscious. Sometimes referred to as “I am nothing””, because the sense of self disappears completely. 

By scaling up (in contemplation), in creates a different mystical experience:

Complete oneness. “I am everything”, profound mystical union with the Universe and everything that is. 

There is no content in either of these experiences. It’s not about knowing something in particular - it’s not possible to understand it intellectually or get it from a book. It’s experiential knowing.

The state of nonduality is both of these experiences at once, “I am nothing” and “I am everything” simultaneously. This is the state that is sought after in many wisdom traditions because it leads to a profound and continuous capacity for insight. 

This is what is referred to as enlightenment, awakening. It’s not an identity you can have, it’s a fundamental way of being. It’s a fundamental insight into what it is to be a human being. Sati. “Buddha” means “awakened one”.

This is a radical transformation in cognition, a complete restructuring of the agent-arena relationship. This is not just an important thing to note historically, it’s something that is achievable with the right training. This is present in many wisdom traditions, not just Buddhism. 

Why do people pursue altered states of consciousness? Psychedelics can produce similar types of transformational states as meditation or contemplation, albeit temporary. Training meditation and contemplation actually increases our capacity for insight, consistently and at will. 

30-40% of the population have had some sort of a mystical experience. This is an important phenomenon. 

There is a deep connection between awakening, insight, and recovering meaning. 

Awakening optimizes our capacity for coherence (making sense). This is deeply meaningful.


# Ep. 8 - Awakening from the Meaning Crisis - The Buddha and "Mindfulness" 

Having vs. Being mode

In the having mode, identity becomes a more political and commodified thing, which allows people to be more manipulable

According to the story, Siddartha Gautama (The Buddha) leaves the palace (a mythological place that represents the having mode) and discovers the suffering of the world - sickness, aging, death

He meets a renunciant, someone who has given up the having mode. He sees a deep inner peace in this man’s eyes. This is a direct introduction to the being more. 

Disillusionment - the loss of illusion, which is profoundly painful and difficult, but also profoundly meaningful. It’s part of the awakening experience. 

Morality depends upon life being meaningful, it sits atop of it. Meaning is deeper than morality. There is more to wisdom than that. 

Siddhartha pursues asceticism, subjecting the body to discomfort and pain through self-denial (the opposite of self-indulgence in the having mode of the palace). These types of 180 swings to extremes are common in people’s lives. 

Trying to annihilate the self still comes from the having mode, it comes from attachment. It’s merely the negation of the same thing as before, not the transcendence of it. Too little is just as bad as too much.

It’s about the middle path, the golden mean. It’s not about a compromising solution, it’s a complete reformation. The being mode rejects both self-indulgence and self-denial. It’s about being connected to the moment. 

Sati - remembering a lost mode of being in an experiential way (participatory knowing), a deep restructuring. Often translated as “mindfulness”. 

We remember this being mode in the same way when we wake up from sleep. This is why Buddha means “the awakened one”, and why we talk about this transformation as an “awakening”. 

Awakening is a way to respond to the meaning crisis. 

If we want to awaken from the meaning crisis, we have to understand what mindfulness originally meant, and what kinds of psychotechnologies were part of it. 

We can get deeply confused about mindfulness if we don’t distinguish between the language of training and the language of explaining.

Training is about helping people acquire the skills. 

The spatial metaphor for memory is wrong, we don’t remember things based on their location in the brain. The spatial metaphor is great for training your memory, but it gets you to understand how memory actually works. 

The language by which we train mindfulness should not be imported into how we study and understand it. I.e. terms like “paying attention to the present moment”, “shining a spotlight” make sense for training, but not for understanding. 

Mindfulness means:
Paying attention to the present moment
Observing things without judgement
Reduces reactivity and increases equanimity
Gaining insight 

Simply understanding this intellectually give you nothing, because it’s about knowing those things experientially.

In order to understand this in a useful way, we need to turn this feature list into a schema. 

The feature list doesn’t explain how the features are related or how they cause each other. 

Being present -> Insight -> 

“Being present” means something like soft vigilance, constantly trying to renew your interest in something, constantly exploring something and opening it up. Getting intimately in contact with something. The string needs to be not too tight and not too loose, like a well tuned string of intelligibility. 

Attention is not a spotlight, it’s a very complex process optimization. Attention makes things more salient. 

Practice means optimizing how you do the thing you are practicing, coordinating multiple cognitive processes and systems to work well together 



# Ep. 7 - Awakening from the Meaning Crisis - Aristotle's World View and Erich Fromm 

Aristotle: wisdom is the cultivation of a virtual engine that regulates your self-making process

Aristotle’s contribution to meaning, wisdom, and self-transcendence. He was a universal genius and wrote comprehensive books on many areas of science. We could argue that he invented science. 

Aristotle thought that rationality was the pinnacle of human cognition, which meant the capacity for reflectively realizing your own capacity for self-deception and correcting it, realizing your capacity for the self-cultivation of character virtues, and be in as deep in contact with reality as possible. 

If you can cause something to be, you have a deeper understanding of it than someone who can merely describe it. This deep understanding is Eidos, the form, the structural functional organization. 

Conformity theory: To know something is to be in conformity with it, to share its form in your mind. This is the original, Biblical meaning of “knowing”. Knowing something changes the structure and functioning of your being. The pattern in our mind matches the pattern in the world. 

This is participatory knowing, as opposed to propositional knowing (a very important, ongoing theme in the lecture series)

How do you know something is real? (reality testing)
The relevant organ of cognition is functioning normally (i.e. brain, emotional intelligence system, sensory organs, etc)
The environment isn’t creating distorting conditions
Other people experienced it the same way (inter subjective agreement)

However, this is not enough. For example, Aristotle had a geocentric worldview (the belief that the Earth was at the center of the world)

He believed that everything is made of basic elements - fire, water, earth, air - and that the properties of everything depended on what proportion of each element something contained. 

Aristotle believed that everything in the cosmos is moving on purpose, trying to get where it belongs. This was deeply meaningful. A beautiful order. 


Our worldview is an inseparable feedback cycle between the agent (our own first-person capacity to perceive and act) and arena (our understanding of the world). This is also one of the key ideas in the lecture series.

This is an existential mode (a way of existing), the process by which we co-identify agent and arena to create a coherent view of reality. Without this relationship, none of our actions have any meaning. 

Meta-meaning system - an existential mode that makes many possible meanings. 

Worldview attunement - the process of refining this relationship between the agent and arena and bringing them into coherence. Without this attunement, we experience our existence as absurd.

Aristotle gives an integrated account of our process of understanding the world (sense making), and our process of finding our place in it (meaning making). Nomological order. 

Mindfulness revolution - a recent movement in Western countries to remedy the meaning crisis through practices like meditation & contemplation, with the goal of enlightenment

Siddartha Gautama (The Buddha) is the central figure of the Axial revolution in the East 

The myth goes that Siddartha’s father coddled his son and tried to remove him from any difficulties or suffering, in an attempt to prevent him from pursuing a religious path.

The palace here represents a certain existential mode (way of being) - having mode.

Having needs - needs that are met by having something, categorizing them, controlling and manipulating them. Relating to things in the “I - it” fashion. Relying mostly on intelligence in order to solve problems. Food, water, oxygen, etc. 
Being needs  - needs that are met by becoming something. Developmental needs. Have to do with creating meaning. Relating to things in the “I - thou” fashion. Becoming mature, virtuous, etc. 

Love is a process of being in reciprocal realization, not a process of having. 

When we try to satisfy our being needs from the having mode, this creates a lot of deep existential confusion and misery. This is a technique that marketers regularly use. I.e. convincing you that you can meet your need for maturity and growth by buying a product. 

Discussion points:
Reality testing
Cognition organs functioning normally
Environment isn’t producing distortion
Other people have the same experience
Confusing having needs with being needs, State vs process
Propositional vs. participatory knowing
The relationship between agent and arena creating a coherent, meaningful worldview
Integrating our process of understanding the world (sense making), and our process of finding our place in it (meaning making)
Plato’s cave in 4k




# Ep. 6 - Awakening from the Meaning Crisis - Aristotle, Kant, and Evolution

Aristotle was a student of Plato. 

He did a better job at accounting for change and used biological metaphors. 

He was interested in how things grow & develop. 

We often find growth & development very meaningful. 

Eidos - the pattern by which we come to know what something is, the structural functional organization, the gestalt. Very hard to put into words. 

Actuality - comes from the root “act”, as opposed to potential

Information - in=formation, actualizing the potential of something  


Q. What makes wood act as a chair, a table, or a ship?
A. Aristotle said it is 'form' or 'eidos'.
The structural functional organisation of the wood makes it act like 'X'.


Newton considered mechanical cause and effect (A causes B, B causes C, etc), but that didn’t account for how living things change. 


Homunculus fallacy explains X using X.
Newtonian linear causality prevents circular explanations, or Homunculus fallacy. 

Living things are self-organizing, self-actualizing, self-transforming. They make use of feedback cycles. Circular causality is inherent to living things. 

For a long time, we had no way of reconciling this gap between physics and biology. 

It wasn’t until recently that we figured out that Newton was ultimately wrong. 

The solution: dynamical systems theory (Alicia Juarrero). 

The important distinction is between causes and constraints. 

Causes are events that make things happen. 

Constraints aren’t events, they are conditions. They don’t make things happen, they make things possible. 

It’s important to note the difference between potentiality and actuality.

The structure of living things shapes the probability of events. That is what makes something a separate living thing - the internal set of probabilities is different than the external set of probabilities.

There are two types of constraints: enabling and selective. 

Darwinian evolution is the first dynamical systems theory that humans invented. The feedback cycle at the core of it is sexual reproduction.

Selective constraints (“virtual governor”) of biological evolution reduce an organism's options like scarcity of resources. There is no evolution without scarcity, according to many evolutionary biologists.

Enabling constraints (“virtual generator”) of biological evolution open up potential - genetic variation.

The cycle of these two forces is what creates change through a feedback cycle (“virtual engine”).


The Darwinian evolutionary theory paid homage to Aristotle. 

The Socratic notion of wisdom was about overcoming self-deception. Plato added a story about how to structure the psyche. 

Aristotle says growth and meaning are missing from this definition of wisdom.

Character - not the personality you’re born with, it’s something you can cultivate. 

Virtue - a set of conditions that have been cultivated systematically. 

I.e. Courage is the golden mean between cowardice and foolhardiness. It’s about finding the right balance. 

To Aristotle, wisdom is the ability to cultivate the character and virtues so that you actualize your potential. 

Ignorance is when you do the wrong thing because you don’t know. Foolishness is when you know, but you do the wrong thing anyway. 

Living things are not just self-organizing, they also seek out the conditions that continue & enhance life. Humans take this even further, where we have the awareness and rationality to choose the direction of our own growth. 

Psyche means self-moving. The mind is the most self-making part of the human organism.


Going up this hierarchy is what it means to “live up to” our potential as humans.
In-form your being with virtual engines distinctive of humanity.
Overcome self deception.
Cultivate character.
Realise wisdom.
Realise the structure of your psyche and your contact with reality.

Your purpose is to become as fully human as possible. 



# Ep. 5 - Awakening from the Meaning Crisis - Plato and the Cave

Socrates was convinced that making a life meaningful means increasing wisdom by decreasing self deception (bullship)

Plato was a student of Socrates, and was one of the most influential thinkers to Western Civilization, his work is foundational to it

Something is sacred when it is an inexhaustible fountain of wisdom and insight

Plato wanted to understand how Athens could have killed Socrates, how people could be so foolish

He took the two worlds mythology and did something different from the Hebrews. He attempted to give a scientific answer. 

He created the first psychological theory in history, a beginning of cognitive science.

Inner conflict: when we have two motives working against each other

Plato gets the insight that there is a deep connection between inner conflict and destructive self deception 

Plato realizes that we have different centers within the psyche, which have different goals and ways of working.

“The man”: resides in the head, the part that is considered with truth, scope, abstraction, long term planning

“The lion”: resides in the chest, the social part, works in terms of honor and shame, works to earn the respect of others, concerned with mid term goals, wants to have shared experience with others. Important for working together with others. 

“The monster”: resides in the stomach and genitals, represents appetite, works in terms of pleasure and pain, immediate goals, superficial, concerned with salience not truth

(Freud divided the self into similar 3 parts.)


The idea is to get this system properly ordered. When it isn’t, we become prone to self deception and get ego-centric

Hyperbolic discounting - how much you are reducing the salience of a stimulus

The more discounting, the less salient something is, the less it stands out, the less it grabs your attention. Something in the present has large salience, remember the monster that overrides the man. Something in the future has high discounting, it loses its salience.

We have to screen out low probability events, otherwise we get cognitive overwhelm

People with anxiety find low probability things too salient

The same machinery that makes you adaptive also makes you prone to self-deception 

“The man” can learn and grasp abstract theory
“The lion” isn’t capable of theory, but it can be trained using reason 

Socrates took reason into the social arena. Social interaction was bonded to social reinforcement. Using this Socratic method, “the man” can train “the lion”, and together they can contain or tame the monster (but never kill it).

This is a way to reduce internal conflict. For Plato, this means to experience a fullness of being, to be at peace with yourself. 

Reducing inner conflict makes self-deception go down, reduces how ego centric we are. We get more in touch with reality. We get better at picking up relevant patterns in reality. We get a vision, that we can then apply to ourselves - self-knowledge. We can then teach “the man”.

You have to change yourself in order to see things more clearly. This is participatory knowing. 

[Note: this is the same process as increasing Sovereignty]


The myth of the cave. We don’t see reality how it is, we only see a shadow of it, until we learn to see real patterns instead of just correlational patterns. 

Waking up to this is a slow process. It’s a participatory transformation. This is the Western myth of enlightenment, self-transformation, liberation, fullness, coming into the light. This process enhances our meaning in life. 

Anagoge - finding inner peace by coming more and more into contact with the real patterns of life; (hermeneutical, ritual) leading up to higher insights, divine visions etc.

The Matrix films present the same story. Neo means “the new man”. This is an archetypal story, a myth that speaks to us across different times in history. 

Reason and spirituality are inseparably bound together here, they are not opposed. 

EIdos - paradigm. The real patterns of life. The pathway of understanding something. 

We often think we understand something by listing its features, but this is not the case. It’s actually the structural functional organization that makes the whole greater than the sum of its parts. In German, it’s called Gestalt. In Greek, it’s Logos.

We often know this structural functional organization intuitively and can’t articulate it verbally. This is participatory knowing. 




# Ep. 4 - Awakening from the Meaning Crisis - Socrates and the Quest for Wisdom

The Axial Revolution - 800-300 BCE 

Soctrates and Plato are the most important thinkers of this era, and are foundational to Western thought

Socrates was very provocative for his time. 

In ancient Greece, people believed they could speak to oracles. One of the most famous was the Oracle of Delphi, where a woman sat in a cave in trance, people would pose questions to her, and she would answer on behalf of the gods. Males would interpret what she had to say. 

The Delphic oracle claims that Socrates is the wisest man alive. Instead of being self-congratulatory, he questions it. 

We are currently in the age of “confirmation porn”. We have technologically enhanced our capacity for gratifying our confirmation bias. We seek echo chambers that reinforce our beliefs instead of challenging our thinking.

Socrates attempt to marry truth & divinity. 

“Know thyself” doesn’t mean becoming aware of our biography, it’s not about ego stroking. It’s a direct participatory knowing of how you operate. It’s like your owners manual - what are the principles, powers, constraints? 

Socrates faces a personal dilemma - how can it be that he is the wisest man, while he knows he is not wise? He starts on a quest to determine how they can both be true. 

Thales invented the kind of thinking that became the scientific method, rationality - rather than the mythological thinking that dominated before him. 

Psyche - self-moving, that which moves things

Ontology - the study of being and the structure of reality using reasoning about the underlying forces

Looking into the depths of reality provokes awe and wonder. That’s what it means to experience something as sacred. 

The natural philosophers gave you the truth, facts, knowledge - but without transformation. They didn’t give you the tools to overcome self deception, gain wisdom, or become a good person. Truth, but no relevance. This is the same criticism we can make of scientific knowledge to this day. 

Ancient Athens treated women very badly. Democracy was only for a certain class of males, but not all. The capacity for debate gave you power in Ancient Greece. 

Rhetoric - a new psychotechnology of Ancient Athens. Standardized skills for influencing people and change their minds. This is disconnected from the truth. 

The sophists were only concerned with teaching and using the skills of rhetoric, without moral commitment (much like lawyers or marketers do today).

Bullshit - a technical term by Harry G. Frankfurt. Lying depends on people’s commitment to the truth. Bullshit works by making you disinterested in the truth, so that you don’t care. This is how advertising works. We can't lie to ourselves but we can bullshit ourselves.

One way to articulate the meaning crisis is the prevalence of bullshit. We are separating salience from truth. 

Lying to yourself isn’t an accurate description of what we do, because it has to do with believing. We don’t voluntarily choose to believe things.

But we bullshit ourselves by directing our attention. This makes it more salient, it stands out to you. By directing your attention, you make things more salient. 

Socratic wisdom 

Socrates knew what he didn’t know, he confronted his capacity for bullshitting himself.

He found a way that reason and love went together (which our culture tends to separate). 
Ta Erotika - know how to love well (not the sexual sense), know what to care about.

Socrates integrated shamanic methods into his practice. He could stand and meditate for 24 hours motionlessly, walk around without shoes in winter, go into battle fearlessly. He had excellent control of his physiology and his mind. 

Socrates was put to death because he was too controversial. He found the pursuit of philosophy so meaningful that he was willing to die for it.

Plato took the ideas of Pythagoras and Socrates and put them together. 


# Ep. 3 - Awakening from the Meaning Crisis - Continuous Cosmos and Modern World Grammar

The Axial Revolution - 800-300 BCE 
Major shift in human history that was formative of us as a Civilization. Literacy and coinage are two of the psychotechnology innovations introduced in the axial revolution that help with new forms of collective intelligence and meta-cognition.

People become more aware of their responsibility for the violence, chaos & suffering in their lives and the world

They understand that transformation is a way to eliminate suffering

Myths are not fictions, they are symbolic stories about important human patterns, which are shareable. The mythological view is neither purely scientific nor metaphorical. 

Continuous Cosmos (Charles Taylor) was a view of reality before the axial age. Human beings experienced themselves in unity with everything, a deep connection between culture, nature, and divinity. Before the axial age, it was not uncommon for people to think that animals are conscious, and that some individuals are god-like.
People enacted myths in order to try to tap into divinity.
They didn’t necessarily want to change their future, time was seen as circular
In this view, wisdom was associated with knowing how to tap into the natural cyclical power to achieve prosperity, long life, security, and conflict free. Meaning is in connectedness. 

During the Axial revolution, aka the great disembedding, this worldview was shattered and replaced by a different one. 

Two Worlds view - the “everyday world” (characterized by foolishness, self-deception, chaos, violence, and suffering) and the “real world” (characterized by wisdom, clear trained view of reality, cutting through the illusion of the everyday world, and reduced suffering). This is not literal, but a mythological representation for the process of self-transformation. 

In this view, wisdom becomes associated with knowing how to make the transformation to the real world. Meaning changes from connectedness to the illusory "everyday world", to a connectedness to the “real world”. Growth becomes an important value. 

The old shamanic practice of “shamanic flight” (looking at yourself and the world from an outside perspective) becomes reused in our capacity for transcendence.

A new idea emerges that we are strangers or pilgrims in the everyday physical world, in which we don't belong. We belong in the real world.

India is the source of the confluence between Eastern and Western philosophy.

A lot of what you think is “natural” is actually culturally internalized. I.e. literacy, abstract thinking. 

The mythology of Two Worlds doesn’t work anymore. We can’t bare to live that way. 

The scientific worldview is returning us to a Continuous Cosmos. There isn’t a difference between us and animals, or between your mind and body. 

We are being re-embedded back into the physical world (continuous cosmos), but how do we make sure we don’t lose what we gained in the disembedding (two world view) - namely, our capacity for self-transcendence? 
This foreshadows part of the meaning crisis. How do we live with the legacy of the axial revolution when we can no longer inhabit its world view.

Grammar is how you put thoughts together, not what you say. Most people believe in God, even if they claim to be atheists (they believe in something absolute, ideal, transcendent, etc). This is what Nietzsche meant that we haven’t gotten rid of God.

Understanding time as a cosmic narrative: Circular (reality is set and repeats itself) vs. Open-ended future (reality is being created by people together with God)

In the Axial age, humans adopt the belief that individual actions can change the future, that they participate in its creation. 

We start talking about time as a progression - we want to grow, improve, transcend

Kairos (from the theologian Paul Tillich) means a crucial turning point.

Participatory knowing - you are changing something as it’s changing you rising to kairos. This is why the Bible uses “knowing” as a euphemism for having sex with someone. During which you are immersed in knowing them like in a stream, you empathise with them, participate, resonate, and identify with them.

Faith didn’t mean believing in things without evidence, it meant being in reciprocal realization, being “on the path”, evolving on course, knowing what to do about your life. 
To trespass used to mean someone thought they were on the path when they weren't. They deceive themselves.

Sin means to miss the mark, to be “off the path”, to fail to participate in the creation of the future 

The Bible is a mythology, a story that encompasses the Axial worldview

A prophet isn’t a psychic who predicts the future, the job of the prophet was to wake you up to how you are “off the path”, how you are deceiving yourself, and how to get back on track.

To be on the path meant a commitment to cultivate the wisdom of deeply remembering God, not reciting beliefs but participating in the ongoing creation of the world, shaping the future by helping others. Progress is measured in moral improvement, increased justice, increased flourishing, people increasingly living up to their promise. This way of thinking came from the Hebrews.

Literacy increased the ease with which people can process information. It increases fluency because they believe it to be more real, they have more confidence in it. 

Cognitive Fluency improves capacity to get into a flow state.

The Greeks invent mathematics and geometry (even though arithmetic was already present before)
They also invent the training for rational argumentation

Many Greek philosophers rediscovered shamanic psycho-technologies, some got shamanic training (i.e. Pythagoras engaged in the thunderstone ceremony of being isolated in a cave and experiencing soul flight).

Pythagoras and Socrates epitomise the axial revolution
Pythagoras discovers that there are mathematic proportions in the world like octaves.
He uses his rational capacity to engage in self-transcendence.
He comes up with the idea that we’re trapped in this physical world, but that we can be free from it through “soul flight” (shamanic flight, self-transcendence). Getting in touch with the rationally realised patterns will liberate us.

He is the first person to describe the Universe as a Cosmos. The root of the word Cosmos is to reveal the beauty and order of things (“cosmetic” comes from the same root)

Pythagoras has the idea of integrating music, mathematical thinking and altered states of consciousness to transcend and see the world as a cosmos.

Awakening in this context is the capacity to see the world as more beautiful and orderly


Discussion:

It is interesting that we identify who we are by the stories we tell ourselves. Yet the very concept of a "narrative" is a psycho technology installed in our conscience by culture. The way we perceive the world is shaped by these psycho technologies without us being aware of it.
The younger generation has a different perception of self that includes the mobile phone demonstrating how technology shapes meaning.
The heros journey is morphing into a collective story based more on reality where people actually die as in Game of Thrones. Disney movies are portraying more of a collective collaboration to reach success instead of an individual hero. This is also evident in board games that require collaboration to save the world.
Post collapse societies have a stronger collective identity.
Sovereignty and collective intelligence are not necessarily exclusive.
Jordan Hall also talks about giving his child an assignment thinking it was unrealistic. After a time he asked how it was going but it hadn't been started. Then in a short space of time other students were co-opted into the project that took ownership of their responsibility resulting in a great outcome far exceeding expectations.
K9 Coliving have higher social interactions than most people. There is a more intuitive caring that ensures a cohesive environment. Christiana in Copenhagen is another example of a long established well structured intentional community.
Kids innately know how to interact with the arena but schools drum it out. It is only later that some learn how to get into coherence with a team as Jamie Wheal talks about with Navy Seals. China has a deeply embedded collective thinking.
A lot of these psycho-technologies persist in our institutions. Some radicals manage to break free.
Given our history of a continuum of western civilization how can we bring an individual purpose into the collective eternal now without experiencing trauma? We are brought together actively listening to each other, learning and contributing to an evolving collective.
Circling allows a way to think from a different frame of view. Story allows a way to deal with trauma but it obscures truth. When the story is removed the trauma can return. Language and enculturation is a type of story that constantly restrain us from revealing our true selves. Society shames those that talk about their feelings, eg. men are taught not to cry - "don't be a baby".
We would like to think climate change is just another narrative that we can control. Oops, it isn't working. More people need to drop the story and see the reality of what is really happening. It is scary. It is disturbing/panic/anxious to wake every day thinking the train of society is heading straight into a wall without being able to do anything about it. What can I do?
The agile methodology allows faster development of products before they become obsolete. This means shorter cycles of continual evaluation and iteration, learning from rapid failures. In a similar way society also needs to become more flexible and adaptable. An agile team of experts leverage off each others skills to solve unseen problems.
What does the next level of circling look like for a collective of sovereign people?
How do we combine the wisdom of returning to cyclical time in a practice? (MAPS)
Shamans have a good understanding of circular time. Many of our understandings come from witches because they were more intune with natural cycles be it menstrual, seasonal, lunar cycles, breath.
Story allows a way of living forward with cyclical rituals and habits that conserve energy thinking about the same tasks. Avoiding decision fatigue frees the mind to focus on something else.
Some accept the world view of their culture and their thinking reflects those world views. By seeing the real world without the imposed illusions of enculturation allows a person to cognitively rise up a level on the spiral. Their mental simulation of the world is closer to reality.
The more viewpoints we learn to think from the more awakened our deeply interconnected collective intelligence becomes.
Jeff Hawkins talks about how we know there isn't a little homunculus in our brain telling us what the eyes see. The brain predicts what it thinks it sees then searches for confirmation settling on the illusion of reality. When the brain sees an optical illusion it creates dissonance. This is analogous to what we think of as identity. It is really an agent arena dance.

# Ep. 2 - Awakening from the Meaning Crisis - Flow, Metaphor, and the Axial Revolution

Upper Paleolithic transition - major upgrade in human cognition. This was formative of us as human beings. 
Driven by shamanism, which helped with hunting and medicine / healing
Shamans used disruptive strategies to get into altered states - social isolation, food deprivation, sex deprivation, chanting, dancing, psychedelics. They used these to disrupt normal cognition and induce different ways of knowing and meaning making. This is known as “breaking frame”
Flow state - when the demands of the situation go slightly beyond our skill or ability. Ex: video games. This state is deeply connected to meaning. Shamanism is a practice for getting into the flow state. 
Our culture has created “flow induction machines”, where our skills are constantly improving and our environment is constantly being upgraded 
Addiction runs off cognitive machinery that is evolutionarily adaptive (“hypernormal stimuli”)
Universal patterns (i.e. shamanism all around the world, people in all cultures seeking flow states) are important because they give us insight into our cognitive machinery
When we’re in a flow state: 
A lot of energy coursing through the body
Sense of time is distorted, often slowing down
Sense of self often disappears
Everything feels more vivid and meaningful
We often do our best work
What you need to get into a flow state:
Skills and demands matched
Tight link between agent and environment (feedback loop)
Failure has to matter
Flow state = insight cascade
People have an intuitive ability to pick up patterns, even if they don’t know how they are doing it
What we call intuition is actually implicit learning - ability to detect complex patterns. Shamans train this ability. 

[Learning by rote, or what Jordan Hall calls Simulated Thinking, appears to be an exaptation of our intuition for deeper complex relationships between ideas. This is fantastic for literacy, numeracy, science and technology, but not so good for noticing what is happening in our bodies, as our cognitive and intuition capabilities pay more attention to learned patterns that are installed by culture. Is this why we say “making sense?” Perhaps a big part of the current sense-making crisis is that we’ve reached a threshold in which it is important to reawaken our native somatic and sensory awareness - comment by David Swedlow].

This ability for intuition isn’t necessarily accurate and is biased - i.e. someone who has racist views will likely have racist intuitions. 
We can pick up on patterns that are correlational or causal
We cannot replace implicit learning with explicit learning. You can’t tell people to pick up on patterns directly - it doesn’t work. What we can do is create the conditions that produce good implicit learning. 

How to train implicit learning: 
Clear information
Tightly couples feedback
Error / failure matters
The same criteria you need to get into a flow state. 
The scientific method also does this. 
The deeper the flow state, the more mystical it feels. Shamans are masters of getting into very deep flow states. 
In these states, you can get different parts of the brain to communicate that normally don’t communicate. This produces novel insights and allows us to make connections between ideas.

All of our cognition is filled with metaphor. Metaphor is how we make creative connections between ideas.  It’s at the heart of science, art, and mysticism. 

Shamanic psycho-technologies improve our capacity to make sense of the world, to make meaning.

Shamanic flight - The capacity to feel “outside” of self and world (metaphorically, not literally), having oversight. This helps see a broader picture. 

The invention of agriculture brought on complex societies. 
People become sedentary. Their relationship to their environment and to themselves changes. 
The Bronze age emerges - first great civilizations in Mesopotamia and Egypt.

Rituals become more complex and sophisticated.

Then, there was the great Bronze age collapse - the biggest in human history. The closest thing we experienced to an apocalypse. More cities and cultures disappear than in any other period. As tragic as this collapse was, it was likely necessary for the birth of the Axial Age.

The Axial Revolution - 800-300 BCE 
Major shift in human history that was formative of us as a Civilization. 

Massive demands on humans cognition to adapt, a bottleneck event in human history. 
People are more likely to experiment with new forms of social organization, new psycho-technologies. Bottle necks may appear as population collapse, or civilizational collapse. One could argue that we are in a cognitive/civilizational collapse currently, as previous meaning making systems are insufficient to making sense of the complexity the world is presenting to us. 

New kind of literacy emerges - alphabetic literacy (one that uses letters instead of hieroglyphs)
Earlier forms of literacy (i.e. cuneiform) were very cumbersome and hard to learn. Very few people used them (only a few professional script writers in each city). Alphabetic literacy was more accessible to the general population. 

Literacy enhances our cognition and sense of self. We become more aware of our own thoughts and can correct our thinking. We can externalize it and store it. 

Metacognition - awareness of your own mind. [One could argue that what is new with this lecture series and in the time of a worldwide internet is collective metacognition].

Second-order thinking - capacity to critically examine and correct your own thinking. This emerged as a result of literacy. 

During the Axial age, empires get rebuilt and form large armies. 
Coinage (money) is invented. 

We learn how to think in abstract symbol systems and in numbers. This is an exaptation of alphabetic literacy. 

These new psycho-technologies increase our ability for self-transcendence and also self-deception (as well as ability to correct self-deception). [At the collective level, we appear to be exapting this machinery further to allow for collective self-transcendence and collective self-deception.] 

People start to develop a sense of responsibility for the world around them. Before this era, people saw chaos and violence as part of the natural world. With second-order thinking, people realize that they themselves create chaos and violence, and can choose to change that. 

[It’s possible that we are now going through a similar realization about our collective effect on the planetary ecosystems]
Discussion Questions
What do you do to get into flow states?
What are the top skills to develop to achieve flow states more easily?
How to be a catalyst for others to achieve flow states?

# Books Episode 2 mentioned:
•Mihalyi Csikszentmihalyi - Flow: The Psychology of Optimal Experience
•Matt Rusano - Supernatural Selection: How Religion Evolved
•Karen Armstrong - The Great Transformation: The Beginning of Our Religious Traditions
•Robert Bellah and Hans Joas (Editors) – The Axial Age and its Consequences
•Eric Cline - 1177 B.C.: The Year Civilization Collapsed
•The Dhammapada
•Robert Drews - The End of the Bronze Age: Changes in Warfare and the Catastrophe ca. 1200 B.C.
•Robin Hogarth – Educating Intuition
•Karl Jaspers - The Origin and Goal of Hist
•George Lakoff and Mark Johnson - Metaphors We Live By
•Steven Pinker - The Better Angels of Our Nature: Why Violence Has Declined
•Arthur Reber - Implicit Learning and Tacit Knowledge: An Essay on the Cognitive Unconscious
•Joseph Schear (Editor) - Mind, Reason, and Being-in-the-World: The McDowell-Dreyfus Debate

---
# Ep. 1 - Awakening from the Meaning Crisis - Introduction by John Vervaeke
Huge interest in mindfulness, wisdom, happiness, meaning.
We're going through a mental health crisis, suicide is spiking.
Increasing sense that people are losing touch with reality. Increased nihilism, frustration, futility. Abandonment of trust in any public institutions, political system, judicial system, religions, clubs, groups, etc.
Increasing sense of bullshit.
A sense that we're spending too much time in our virtual environments.

New mythological forms (zombies and superheroes).
Everyone is talking about crisis and collapse of civilization.

Wisdom is about realizing meaning and life in a profound way.

Why meaning, wisdom, and self-transcendence are all connected to the awakening from the meaning crisis

Foolishness (as opposed to wisdom) is when agency is undermined by self-destructive behavior and bullshit. Same machinery that makes us adaptive/wise potentially makes us foolish.

Different kinds of knowing - some that have fallen off our cultural radar. Modern meaning only includes holding a special kind of justified belief, but there is much more to it.

Sense of self, sense of realness, have to be transformed. (Other kinds of knowing.)

It’s not possible to find an answer to the meaning crisis in an hour; it’s a very complex thing. 

Meaning-making is deeply wired into our cognition and our evolutionary heritage. 

Around 40,000 BCE, during the Upper Paleolithic era, humanity went through a radical transition. Humans start making representational art - sculpture, cave paintings, music. Significant enhancement in cognition (frontal lobe) - calendars, abstract representation of patterns that helped with hunting, development of projectile weapons (spears).

This is still reflected in our language and how we think about goals. The subject (you) is doing a project (i.e. throwing) towards an object/objective (thing you are throwing towards)

Humanity went through a near-extinction event around the end of the last ice age, possibly due to a supervolcano (dates?). Humanity reduced to around 10,000 individuals. This produces an evolutionary response - a sociocognitive response. Wider trading networks emerge (reduced the effects of variation in local factors).

Humans develop rituals for dealing with the enhanced social networks. This includes handshaking, perspective taking (mind sight).

Our cognition is participatory - large networked cognition that includes other humans

Humans developed the ability to communicate and develop trust relationships with strangers.

As we increase our understanding of the mental states of others, we increase self awareness.  

We become less loyal to our group because there is often temptation from the strangers. The solution to this was the invention of the initiation ritual, which includes facing pain, fear, etc - to prove commitment to the group. 

Humans develop the ability to better regulate their emotions and let go of their ego for the benefit of the group. 

Exaptation - new adaptation of a body part that was originally used for something else (i.e. the tongue evolved for eating, we later reused it for speech). We do this with our brain networks all the time.

Shamanic rituals - cultivated practice for altering our states of consciousness. The shaman is an archetypal figure (wise old man/woman, i.e. Yoda). Becoming aware of the mind, controlling and training states of consciousness and emotions. Shamans engage in sleep and food deprivation, imitating animals, social isolation, psychedelic use - strategies to bring about transformative experiences. These disrupts the way you find patterns in the world, your framing. 

Shamanism was very central for the Upper Paleolithic transition. Humans did not go through a hardware change, our brains stayed the same. It was a software change in how we used the brain. 

Psychotechnology - Exaptation from physical to cognitive tool, enhances how the brain works (like Steve Jobs’ bicycle for the mind).

Literacy - a standard set of tools for how we process information 

Installing new beliefs does not improve people’s problem solving abilities. It’s about showing them how to alter their perception, attention, sense of reality. Shamanism does this to produce enhanced insight into patterns in the environment. Pretending to be a deer engages participatory knowing of the deer and assists with tracking; it isn’t propositional knowing.

The shaman manipulates the meaning of things. Induces the placebo effect. (Could this indicate that the shaman would have to “fake it” to activate the placebo effect? Could not reveal hidden knowledge at risk of the spell being broken?)

“Shaman” means one who knows, one who sees. A wizard, wise person.

# Books Episode 1 mentioned:

Supernatural Selection: How Religion Evolved by Matthew J. Rossano
Mindsight: The New Science of Personal Transformation by Daniel J. Siegel
After Phrenology: Neural Reuse and the Interactive Brain by Michael L. Anderson 
Shamanism: A Biopsychosocial Paradigm of Consciousness and Healing by Michael J. Winkelman
Natural Born Cyborgs by Andy Clark
Waking from Sleep: Why Awakening Experiences Occur and How to Make Them Permanent  by Steve Taylor
The Madness of Crowds: Gender, Race and Identity by Douglas Murray
Meaning in Life and Why It Matters by Susan Wolf
Zombies in Western Culture: A Twenty-First Century Crisis by John Vervaeke, Christopher Mastropietro, and Filip Miscevic
The Mind in the Cave: Consciousness and the Origins of Art by David Lewis-Williams
On Bullshit by Harry Frankfurt
The Mindfulness Revolution: Leading Psychologists, Scientists, Artists, and Meditation Teachers on the Power of Mindfulness in Daily Life - Barry Boyce (Editor)
The Scientific Study of Personal Wisdom: From Contemplative Traditions to Neuroscience - Michel Ferrari and Nic Weststrate (Editors)
Transformative Experience by L. A. Paul
